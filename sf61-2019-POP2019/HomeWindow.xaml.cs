﻿using sf61_2019_POP2019.Model;
using sf61_2019_POP2019.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();

            UpdateView();
        }
        private void UpdateView()
        {
            cmbTipKorisnika.ItemsSource = new List<ETipKorisnika>() { ETipKorisnika.ADMINISTRATOR, ETipKorisnika.LEKAR, ETipKorisnika.PACIJENT };

            Util.Instance.UcitajAdreseIzBaze();
            Util.Instance.UcitajAdministratoreIzBaze();
            Util.Instance.UcitajDzIzBaze();
            Util.Instance.UcitajLjekareIzBaze();
            Util.Instance.UcitajPacijenteIzBaze();
            Util.Instance.UcitajTerapijeIzBaze();
        }

        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            /*
            if (!(String.IsNullOrEmpty(txtJMBG.Text)) && !(String.IsNullOrEmpty(lozinkaBox.Password)) && cmbTipKorisnika.SelectedIndex >= 0)
            {
                bool logovanjeUspjesno = Util.ValidirajKorisnika(txtJMBG.Text, lozinkaBox.Password, (ETipKorisnika)cmbTipKorisnika.SelectedItem);

                if (logovanjeUspjesno == true)
                {
                    Korisnik kor = Util.PretraziPoJMBG(txtJMBG.Text);

                    if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.ADMINISTRATOR))
                    {
                        var logged = kor as Administrator;
                        AdministratorWindow aw = new AdministratorWindow(logged);
                        aw.Show();
                        Close();

                    }
                    else if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.LEKAR))
                    {
                        var logged = kor as Lekar;
                        LjekarWindow lw = new LjekarWindow(logged);
                        lw.Show();
                        Close();
                    }
                    else if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.PACIJENT))
                    {
                        var logged = kor as Pacijent;
                        PacijentWindow pw = new PacijentWindow(logged);
                        pw.Show();
                        Close();

                    }
                }
                else
                {
                    MessageBox.Show("Unijeli ste pogresne podatke", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else if (String.IsNullOrEmpty(txtJMBG.Text) || String.IsNullOrWhiteSpace(lozinkaBox.Password) || cmbTipKorisnika.SelectedIndex == -1)
            {
                MessageBox.Show("Niste popunili sva polja", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            */

             
            string jmbg = txtJMBG.Text.Trim();
            string password = lozinkaBox.Password;

            bool ulogovan = Util.ValidirajKorisnika(txtJMBG.Text, lozinkaBox.Password, (ETipKorisnika)cmbTipKorisnika.SelectedItem); ;

            foreach (var korisnik in Util.Korisnici)
            {
                if (ulogovan == true)
                {
                    if(!(String.IsNullOrEmpty(txtJMBG.Text)) && !(String.IsNullOrEmpty(lozinkaBox.Password)) && cmbTipKorisnika.SelectedIndex >= 0)
                    {
                        if (korisnik.JMBG.Trim().Equals(jmbg) && korisnik.Lozinka.Trim().Equals(password))
                        {
                            if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.ADMINISTRATOR))
                            {
                                var logged = korisnik as Administrator;
                                AdministratorWindow aw = new AdministratorWindow(logged);
                                aw.Show();
                                Close();
                            }

                            else if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.LEKAR))
                            {
                                var logged = korisnik as Lekar;
                                LjekarWindow lw = new LjekarWindow(logged);
                                lw.Show();
                                Close();
                            }

                            else if (cmbTipKorisnika.SelectedItem.Equals(ETipKorisnika.PACIJENT))
                            {
                                var logged = korisnik as Pacijent;
                                PacijentWindow pw = new PacijentWindow(logged);
                                pw.Show();
                                Close();
                            }

                            break;

                        }
                    } else
                    {
                        MessageBox.Show("Niste popunili sva polja", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }

            if(ulogovan)
            {
                MessageBox.Show("Uspjesno ste se ulogovali", "Cestitamo", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Unijeli ste pogresne podatke", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
        }

        private void txtGost_MouseDown(object sender, MouseButtonEventArgs e)
        {
            NeprijavljenKorisnikWindow nkw = new NeprijavljenKorisnikWindow();
            nkw.ShowDialog();
        }
    }
}
