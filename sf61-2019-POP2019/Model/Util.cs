﻿using sf61_2019_POP2019.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sf61_2019_POP2019.Model
{
    public sealed class Util
    {
        public static Korisnik aktivanKorisnik;
        public ObservableCollection<Adresa> Adrese { get; set; }
        public static ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Administrator> Administratori { get; set; }
        public ObservableCollection<Lekar> Lekari { get; set; }
        public ObservableCollection<Pacijent> Pacijenti { get; set; }
        public ObservableCollection<DomZdravlja> DomoviZdravlja { get; set; }
        public ObservableCollection<Terapija> Terapije { get; set; }
        public ObservableCollection<Termin> Termini { get; set; }
        //konekcija ka bazi 
        public static string CONNECTION_STRING = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=msdb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static Util instance = null;
        private Util()
        {
            Adrese = new ObservableCollection<Adresa>();
            Lekari = new ObservableCollection<Lekar>();
            DomoviZdravlja = new ObservableCollection<DomZdravlja>();
            Administratori = new ObservableCollection<Administrator>();
            Korisnici = new ObservableCollection<Korisnik>();
            Pacijenti = new ObservableCollection<Pacijent>();
            Terapije = new ObservableCollection<Terapija>();
            Termini = new ObservableCollection<Termin>();

            UcitajAdreseIzBaze();
            UcitajAdministratoreIzBaze();
            UcitajLjekareIzBaze();
            UcitajDzIzBaze();
            UcitajPacijenteIzBaze();
            UcitajTerapijeIzBaze();
            UcitajTermineIzBaze();
        }

        public static Korisnik PretraziPoJMBG(string jmbg)
        {
            foreach (Korisnik kor in Korisnici)
            {
                if (kor.KorisnickoIme.Equals(jmbg))
                    return kor;
            }

            return null;
        }

        public static bool ValidirajKorisnika(string jmbg, string password, ETipKorisnika tipKorisnika)
        {
            bool result = false;
            ETipKorisnika tip = new ETipKorisnika();

            foreach (Korisnik kor in Korisnici)
            {
                if (kor.JMBG.Equals(jmbg) && kor.Lozinka.Equals(password) &&
                    kor.TipKorisnika.Equals(tipKorisnika))
                {
                    result = true;
                    tip = kor.TipKorisnika;
                }
            }

            return result;
        }

        public static Util Instance
        {
            get
            {
                if (instance == null)
                    instance = new Util();
                return instance;
            }
                
        }

        public void UcitajAdministratoreIzBaze()
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                Administratori.Clear();
                conn.Open();

                DataSet dataSet = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from Administratori where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(dataSet, "Administratori");

                foreach (DataRow row in dataSet.Tables["Administratori"].Rows)
                {
                    Administrator administrator = new Administrator();

                    administrator.ID = (int)row["ID"];
                    administrator.Ime = (string)row["Ime"];
                    administrator.Prezime = (string)row["Prezime"];
                    administrator.KorisnickoIme = (string)row["KorisnickoIme"];
                    administrator.Email = (string)row["Email"];
                    administrator.Lozinka = (string)row["Lozinka"];
                    administrator.Aktivan = (bool)row["Aktivan"];
                    administrator.JMBG = (string)row["JMBG"];
                    administrator.Pol = VratiPol((int)row["Pol"]);
                    administrator.TipKorisnika = VratiKorisnika((int)row["TipKorisnika"]);

                    Administratori.Add(administrator);
                    Korisnici.Add(administrator);
                }
            }
        }

        public void UcitajAdreseIzBaze()
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                Adrese.Clear();
                conn.Open();

                DataSet ds = new DataSet();

                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from Adrese where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = command;
                adapter.Fill(ds, "Adrese");

                foreach (DataRow row in ds.Tables["Adrese"].Rows)
                {
                    Adresa adresa = new Adresa();

                    adresa.ID = (int)row["ID"];
                    adresa.Ulica = (string)row["Ulica"];
                    adresa.Broj = (string)row["Broj"];
                    adresa.Grad = (string)row["Grad"];
                    adresa.Drzava = (string)row["Drzava"];
                    adresa.Aktivan = (bool)row["Aktivan"];

                    Adrese.Add(adresa);
                }
            }
        }

        public void UcitajDzIzBaze()
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                DomoviZdravlja.Clear();
                conn.Open();

                DataSet ds = new DataSet();

                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from DomZdravlja where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = command;
                adapter.Fill(ds, "DomZdravlja");

                foreach (DataRow row in ds.Tables["DomZdravlja"].Rows)
                {
                    DomZdravlja dz = new DomZdravlja();

                    dz.ID = (int)row["ID"];
                    dz.NazivInstitucije = (string)row["NazivInstitucije"];
                    dz.Aktivan = (bool)row["Aktivan"];
                    dz.Adresa = VratiAdresu((int)row["IDAdrese"]);

                    DomoviZdravlja.Add(dz);
                }
            }
        }

        public void UcitajLjekareIzBaze()
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                Lekari.Clear();
                conn.Open();

                DataSet dataSet = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from Lekari where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(dataSet, "Lekari");

                foreach (DataRow row in dataSet.Tables["Lekari"].Rows)
                {
                    Lekar lekar = new Lekar();

                    lekar.ID = (int)row["ID"];
                    lekar.Ime = (string)row["Ime"];
                    lekar.Prezime = (string)row["Prezime"];
                    lekar.KorisnickoIme = (string)row["KorisnickoIme"];
                    lekar.Email = (string)row["Email"];
                    lekar.Lozinka = (string)row["Lozinka"];
                    lekar.Aktivan = (bool)row["Aktivan"];
                    lekar.JMBG = (string)row["JMBG"];
                    lekar.Pol = VratiPol((int)row["Pol"]);
                    lekar.DomZdravlja = VratiUstanovu((int)row["IDDomaZdravlja"]);
                    lekar.Adresa = VratiAdresu((int)row["IDAdrese"]);
                    
                    Lekari.Add(lekar);
                    Korisnici.Add(lekar);
                }
            }
        }

        public void UcitajTermineIzBaze()
        {
            using(SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                Termini.Clear();
                conn.Open();

                DataSet dataSet = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from Termini where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(dataSet, "Termini");

                foreach(DataRow row in dataSet.Tables["Termini"].Rows)
                {
                    Termin termin = new Termin();
                    termin.ID = (int)row["ID"];
                    termin.DatumTermina = (string)row["DatumTermina"];
                    termin.StatusTermina = VratiStatusTermina((int)row["StatusTermina"]);
                    termin.Aktivan = (bool)row["Aktivan"];
                    termin.DomZdravljaTermin = VratiUstanovu((int)row["DomZdravljaID"]);
                    termin.LjekarTermin = VratiLjekara((int)row["LjekarID"]);
                    termin.PacijentTermin = VratiPacijenta((int)row["PacijentID"]);

                    Termini.Add(termin);

                }
            }
        }

        public void UcitajPacijenteIzBaze()
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                Pacijenti.Clear();
                conn.Open();

                DataSet dataSet = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.Connection = conn;
                command.CommandText = "select * from Pacijenti where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(dataSet, "Pacijenti");

                foreach (DataRow row in dataSet.Tables["Pacijenti"].Rows)
                {
                    Pacijent pacijent = new Pacijent();

                    pacijent.ID = (int)row["ID"];
                    pacijent.Ime = (string)row["Ime"];
                    pacijent.Prezime = (string)row["Prezime"];
                    pacijent.KorisnickoIme = (string)row["KorisnickoIme"];
                    pacijent.Email = (string)row["Email"];
                    pacijent.Lozinka = (string)row["Lozinka"];
                    pacijent.Aktivan = (bool)row["Aktivan"];
                    pacijent.JMBG = (string)row["JMBG"];
                    pacijent.Pol = VratiPol((int)row["Pol"]);
                    pacijent.Ljekar = VratiLjekara((int)row["IDLekara"]);
                    pacijent.Adresa = VratiAdresu((int)row["IDAdrese"]);

                    Pacijenti.Add(pacijent);
                    Korisnici.Add(pacijent);
                }
            }
        }

        public void UcitajTerapijeIzBaze()
        {
            using (SqlConnection connection = new SqlConnection(Util.CONNECTION_STRING))
            {
                Terapije.Clear();
                connection.Open();

                DataSet dataSet = new DataSet();
                SqlCommand command = connection.CreateCommand();
                command.Connection = connection;
                command.CommandText = "select * from Terapije where Aktivan=1";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(dataSet, "Terapije");

                foreach(DataRow row in dataSet.Tables["Terapije"].Rows)
                {
                    Terapija terapija = new Terapija();
                    terapija.ID = (int)row["ID"];
                    terapija.OpisTerapije = (string)row["OpisTerapije"];
                    terapija.Aktivan = (bool)row["Aktivan"];
                    terapija.Ljekar = VratiLjekara((int)row["IDLekara"]);
                    terapija.Pacijent = VratiPacijenta((int)row["IDPacijenta"]);

                    Terapije.Add(terapija);
                }
            }
        }

        private Adresa VratiAdresu(int idAdrese)
        {
            Adresa adresa = null;

            foreach(Adresa ad in Adrese)
            {
                if(ad.ID.Equals(idAdrese))
                {
                    adresa = ad;
                }
            }

            return adresa;
        }

        private DomZdravlja VratiUstanovu(int idDomaZdravlja)
        {
            DomZdravlja domZdravlja = null;
            foreach (DomZdravlja dz in DomoviZdravlja)
            {
                if (dz.ID.Equals(idDomaZdravlja))
                {
                    domZdravlja = dz;
                }
            }

            return domZdravlja;
        }

        private Pacijent VratiPacijenta(int idPacijenta)
        {
            Pacijent pacijent = null; 
            foreach(Pacijent p in Pacijenti)
            {
                if(p.ID.Equals(idPacijenta))
                {
                    pacijent = p;
                }
            }

            return pacijent;
        }

        private Lekar VratiLjekara(int idLekara)
        {
            Lekar lekar = null;
            foreach(Lekar lk in Lekari)
            {
                if (lk.ID.Equals(idLekara))
                {
                    lekar = lk;
                }
            }

            return lekar;
        }

        public static EStatusTermina VratiStatusTermina(int status)
        {
            if(status == 0)
            {
                return EStatusTermina.SLOBODAN;
            }
            else
            {
                return EStatusTermina.ZAKAZAN;
            }
        }

        public static EPol VratiPol(int p)
        {
            if (p == 0)
            {
                return EPol.Musko;
            }
            else
            {
                return EPol.Zensko;
            }
        }

        public static ETipKorisnika VratiKorisnika(int k)
        {
            if (k == 0)
            {
                return ETipKorisnika.ADMINISTRATOR;
            }
            else if (k == 1)
            {
                return ETipKorisnika.LEKAR;
            }
            else
            {
                return ETipKorisnika.PACIJENT;
            }
        }
    }
}

