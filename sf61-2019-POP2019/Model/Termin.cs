﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public class Termin: INotifyPropertyChanged, ICloneable, IDataErrorInfo

    {
        public Dictionary<string, string> ErrorCollection { get; private set; } = new Dictionary<string, string>();

        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string datumTermina;

        public string DatumTermina
        {
            get { return datumTermina; }
            set { datumTermina = value;OnPropertyChanged("DatumTermina"); }
        }


        private EStatusTermina statusTermina;

        public EStatusTermina StatusTermina
        {
            get { return statusTermina; }
            set { statusTermina = value; OnPropertyChanged("StatusTermina"); }
        }

        private DomZdravlja domZdravljaTermin;

        public DomZdravlja DomZdravljaTermin
        {
            get { return domZdravljaTermin; }
            set { domZdravljaTermin = value; OnPropertyChanged("DomZdravljaTermin"); }
        }

        private Pacijent pacijentTermin;

        public Pacijent PacijentTermin
        {
            get { return pacijentTermin; }
            set { pacijentTermin = value; OnPropertyChanged("PacijentTermin"); }
        }

        private Lekar ljekarTermin;

        public Lekar LjekarTermin
        {
            get { return ljekarTermin; }
            set { ljekarTermin = value; OnPropertyChanged("LjekarTermin"); }
        }

    
        private bool aktivan;
        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public string Error { get { return null; } }


        public object Clone()
        {
            Termin kopijaTermin = new Termin();
            kopijaTermin.ID = ID;
            kopijaTermin.datumTermina = DatumTermina;
            kopijaTermin.LjekarTermin = LjekarTermin;
            kopijaTermin.PacijentTermin = PacijentTermin;
            kopijaTermin.StatusTermina = StatusTermina;
            kopijaTermin.Aktivan = Aktivan;

            return kopijaTermin;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String propertyName)
        { 
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static void DodajTermin(Termin termin)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Termini(DatumTermina, StatusTermina, Aktivan, DomZdravljaID, LjekarID, PacijentID)" + 
                        "values (@DatumTermina, @StatusTermina,@Aktivan,@DomZdravljaID,@LjekarID,@PacijentID)";

                command.Parameters.Add(new SqlParameter("@DatumTermina", termin.DatumTermina));
                //provjeriti ovdje
                command.Parameters.Add(new SqlParameter("@StatusTermina", termin.StatusTermina));
                command.Parameters.Add(new SqlParameter("@Aktivan", termin.Aktivan));
                command.Parameters.Add(new SqlParameter("@DomZdravljaID", termin.DomZdravljaTermin.ID));
                command.Parameters.Add(new SqlParameter("@LjekarID", termin.LjekarTermin.ID));
                command.Parameters.Add(new SqlParameter("@PacijentID", termin.PacijentTermin.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajTermineIzBaze();
        }

        public static void ObrisiTermin(Termin termin)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Termini set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", termin.ID));
                command.Parameters.Add(new SqlParameter("@DatumTermina", termin.DatumTermina));
                command.Parameters.Add(new SqlParameter("@StatusTermina", termin.StatusTermina));
                command.Parameters.Add(new SqlParameter("@Aktivan", termin.Aktivan));
                command.Parameters.Add(new SqlParameter("@DomZdravljaID", termin.DomZdravljaTermin.ID));
                command.Parameters.Add(new SqlParameter("@LjekarID", termin.LjekarTermin.ID));
                command.Parameters.Add(new SqlParameter("@PacijentID", termin.PacijentTermin.ID));

                command.ExecuteNonQuery();

            }

            Util.Instance.UcitajTermineIzBaze();
        }

        public static void IzmijeniTermin(Termin termin)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Termini set DatumTermina=@DatumTermina, StatusTermina=@StatusTermina, Aktivan=@Aktivan, DomZdravljaID=@DomZdravljaID, LjekarID=@LjekarID, PacijentID=@PacijentID where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", termin.ID));
                command.Parameters.Add(new SqlParameter("@DatumTermina", termin.DatumTermina));
                command.Parameters.Add(new SqlParameter("@StatusTermina", termin.StatusTermina));
                command.Parameters.Add(new SqlParameter("@Aktivan", termin.Aktivan));
                command.Parameters.Add(new SqlParameter("@DomZdravljaID", termin.DomZdravljaTermin.ID));
                command.Parameters.Add(new SqlParameter("@LjekarID", termin.LjekarTermin.ID));
                command.Parameters.Add(new SqlParameter("@PacijentID", termin.PacijentTermin.ID));

                command.ExecuteNonQuery();

            }

            Util.Instance.UcitajTermineIzBaze();

        }

        public string this[string columnName]
        {
            get
            {
                string result = null; 

                switch(columnName)
                {
                    case "DatumTermina":
                        if (string.IsNullOrWhiteSpace(DatumTermina))
                            result = "Datum termina mora biti unesen! ";
                        else if (!Regex.IsMatch(DatumTermina, @"\d?[\/|\-|\s]?\d?[\/|\-|\s]\d(\s?)[\/|\-|\s]?\d?(\s?)[\/|\-|\s]\d{4}[\s]?$", RegexOptions.IgnoreCase))
                            result = "Pogresan format datuma!";
                        break;
                }

                if (ErrorCollection.ContainsKey(columnName))
                    ErrorCollection[columnName] = result;
                else if (result != null)
                    ErrorCollection.Add(columnName, result);

                OnPropertyChanged("ErrorCollection");

                return result;
            }
        }

    }
}
