﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{

    //postaviti on property changed i ostalo u adresu 
    public class Adresa : IDataErrorInfo,INotifyPropertyChanged,ICloneable
    {

        private int _id; 
        public int ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("ID"); }
        }

        private string _ulica;

        public string Ulica
        {
            get { return _ulica; }
            set { _ulica = value; OnPropertyChanged("Ulica"); }
        }

        private string _broj;

        public string Broj
        {
            get { return _broj; }
            set { _broj = value; OnPropertyChanged("Broj"); }
        }

        private string _grad;

        public string Grad
        {
            get { return _grad; }
            set { _grad = value; OnPropertyChanged("Grad"); }
        }

        private string _drzava; 
        
        public string Drzava
        {
            get { return _drzava; }
            set { _drzava = value; OnPropertyChanged("Drzava"); }
        }

        private bool _aktivan;

      
        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public string Error
        {
            get
            {
                return "greska"; 
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch(columnName)
                {
                    case "Ulica":
                        if (string.IsNullOrEmpty(Ulica))
                            return "Ulica je obavezno polje!";
                        else if (Ulica.Length < 2)
                            return "Ulica mora da ima najmanje dva karaktera";
                        break;

                    case "Grad":
                        if (string.IsNullOrEmpty(Grad))
                            return "Grad je obavezno polje!";
                        else if (Grad.Length < 2)
                            return "Grad mora da ima najmanje dva karaktera!";
                        break;

                    case "Drzava":
                        if (string.IsNullOrEmpty(Drzava))
                            return "Drzava je obavezno polje!";
                        else if (Drzava.Length < 2)
                            return "Drzava mora da ima najmanje dva karaktera! "; 
                        break;
                    case "Broj":
                        if (string.IsNullOrEmpty(Broj))
                            return "Broj je obavezno polje!";
                        else if (Broj.Length < 1)
                            return "Broj mora da ima najmenje jednu cifru! "; 
                        break;
                }
                return String.Empty;
            }
        }

        public override string ToString()
        {
             return Grad + ", " + Ulica + ", " + Broj + " ," + Drzava; 
            //return Grad;
        }
       
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static void DodajAdresu(Adresa adresa)
        {
            using (SqlConnection connection = new SqlConnection(Util.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "insert into Adrese (Ulica,Broj,Grad,Drzava,Aktivan) values (@Ulica,@Broj,@Grad,@Drzava,@Aktivan)";
                command.Parameters.Add(new SqlParameter("@Ulica", adresa.Ulica));
                command.Parameters.Add(new SqlParameter("@Broj", adresa.Broj));
                command.Parameters.Add(new SqlParameter("@Grad", adresa.Grad));
                command.Parameters.Add(new SqlParameter("@Drzava", adresa.Drzava));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));
                //command.Parameters.Add(new SqlParameter("@IDLekar", adresa.KorisnikAdresa));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajAdreseIzBaze();
        }
        public static void IzmijeniAdresu(Adresa adresa)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update Adrese set Ulica=@Ulica, Broj=@Broj, Grad=@Grad, Drzava=@Drzava, Aktivan=@Aktivan where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", adresa.ID));
                command.Parameters.Add(new SqlParameter("@Ulica", adresa.Ulica));
                command.Parameters.Add(new SqlParameter("@Broj", adresa.Broj));
                command.Parameters.Add(new SqlParameter("@Grad", adresa.Grad));
                command.Parameters.Add(new SqlParameter("@Drzava", adresa.Drzava));
                command.Parameters.Add(new SqlParameter("@Aktivan", adresa.Aktivan));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajAdreseIzBaze();
        }

        public object Clone()
        {
            Adresa kopija = new Adresa();

            kopija.ID = this.ID;
            kopija.Ulica = this.Ulica;
            kopija.Broj = this.Broj;
            kopija.Grad = this.Grad;
            kopija.Drzava = this.Drzava;
            kopija.Aktivan = this.Aktivan;

            return kopija;
        }

        public Adresa()
        {

        }
    }
}
