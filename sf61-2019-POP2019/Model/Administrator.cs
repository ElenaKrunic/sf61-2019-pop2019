﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public class Administrator : Korisnik, INotifyPropertyChanged, ICloneable, IDataErrorInfo
    {
        public Administrator() : base()
        {
            this.TipKorisnika = ETipKorisnika.ADMINISTRATOR;
            this.Aktivan = Aktivan;
        }
        public string toString()
        {
            return base.ToString();
        }

        public override object Clone()
        {
            Administrator kopija = new Administrator();

            kopija.Ime = Ime;
            kopija.Prezime = Prezime;
            kopija.KorisnickoIme = KorisnickoIme;
            kopija.Email = Email;
            kopija.Adresa = Adresa;
            kopija.Aktivan = Aktivan;
            kopija.JMBG = JMBG;
            kopija.Lozinka = Lozinka;
            kopija.Pol = Pol;
            kopija.TipKorisnika = TipKorisnika;

            return kopija;
        }

        public static void DodajAdministratora(Administrator administrator)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Administratori(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika)" +
                    "values (@Ime,@Prezime,@KorisnickoIme,@Email,@Lozinka,@Aktivan,@JMBG,@Pol,@TipKorisnika)";

                command.Parameters.Add(new SqlParameter("@Ime", administrator.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", administrator.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", administrator.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", administrator.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", administrator.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", administrator.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", administrator.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", administrator.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", administrator.TipKorisnika));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajAdministratoreIzBaze();
        }
        public static void ObrisiAdministratora(Administrator selectedAdmin)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Lekari set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", selectedAdmin.ID));
                command.Parameters.Add(new SqlParameter("@Ime", selectedAdmin.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", selectedAdmin.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", selectedAdmin.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", selectedAdmin.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", selectedAdmin.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", selectedAdmin.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", selectedAdmin.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", selectedAdmin.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", selectedAdmin.TipKorisnika));

                command.ExecuteNonQuery();

            }
            Util.Instance.UcitajAdministratoreIzBaze();
        }


        public static void IzmijeniAdministrator(Administrator administrator)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Administratori set Ime=@Ime, Prezime=@Prezime, KorisnickoIme=@KorisnickoIme, Email=@Email, Lozinka=@Lozinka," +
                                        "Aktivan=@Aktivan, JMBG=@JMBG, Pol=@Pol, TipKorisnika=@TipKorisnika WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", administrator.ID));
                command.Parameters.Add(new SqlParameter("@Ime", administrator.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", administrator.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", administrator.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", administrator.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", administrator.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", administrator.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", administrator.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", administrator.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", administrator.TipKorisnika));

                command.ExecuteNonQuery();
            }
            Util.Instance.UcitajAdministratoreIzBaze();
        }

    }
}


