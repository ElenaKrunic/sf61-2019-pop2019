﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public abstract class Korisnik : IDataErrorInfo,INotifyPropertyChanged, ICloneable
    {
        public abstract object Clone();

        public string Error { get { return null; } }

        public Dictionary<string, string> ErrorCollection { get; private set; } = new Dictionary<string, string>();

        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _korisnickoIme; 
        public string KorisnickoIme
        {
            get { return _korisnickoIme;  }
            set 
            {
                _korisnickoIme = value;
                OnPropertyChanged("KorisnickoIme");
            }
        }

        private string _ime; 
        public string Ime
        {
            get { return _ime; }
            set { _ime = value; OnPropertyChanged("Ime"); }
        }

        private string _prezime; 
        public string Prezime
        {
            get { return _prezime;  }
            set { _prezime = value; OnPropertyChanged("Prezime"); }
        }

        private string _lozinka; 
        public string Lozinka
        {
            get { return _lozinka; }
            set { _lozinka = value; OnPropertyChanged("Lozinka"); }
        }


        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _jmbg;
        public string JMBG
        {
            get { return _jmbg; }
            set { _jmbg = value; OnPropertyChanged("JMBG"); }
        }

        private Adresa _adresa;
        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; OnPropertyChanged("Adresa");}
        }

        private EPol _pol;
        public EPol Pol
        {
            get { return _pol; }
            set { 
                _pol = value;
                OnPropertyChanged("Pol");
            }
        }

        private ETipKorisnika _tipKorisnika;
        public ETipKorisnika TipKorisnika
        {
            get { return _tipKorisnika; }
            set { _tipKorisnika = value; OnPropertyChanged("TipKorisnika"); }
        }

        private bool _aktivan;
        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        public Korisnik()
        {

        }
        public override string ToString()
        {
            return Ime + " " + Prezime;
        } 
 
          public virtual string this[string columnName]
          {
              get
              {
                string result = null;

                 switch(columnName)
                 {
                    case "Ime":
                        if (string.IsNullOrWhiteSpace(Ime))
                            result = "Ime ne moze biti prazno polje!";
                        else if (Ime.Length < 2)
                            result = "Ime mora sadrzati bar dva karaktera!";
                        break;

                    case "Prezime":
                        if (string.IsNullOrWhiteSpace(Prezime))
                            result = "Prezime ne moze biti prazno polje!";
                        else if (Prezime.Length < 2)
                            result = "Prezime mora sadrzati bar dva karaktera!";
                        break;

                    case "Email":
                        if (string.IsNullOrWhiteSpace(Email))
                            result = "Email ne moze biti prazno polje!";
                        else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                            result = "Pogresan email format!";
                        break;

                    case "KorisnickoIme":
                        if (string.IsNullOrWhiteSpace(KorisnickoIme))
                            result = "Korisnicko ime ne moze biti prazno polje!";
                        else if (KorisnickoIme.Length < 5)
                            result = "Korisnicko ime mora da sadrzi bar 5 karaktera!";
                        break;

                    case "Lozinka":
                        if (string.IsNullOrWhiteSpace(Lozinka))
                            result = "Lozinka ne moze biti prazno polje!";
                        else if (Lozinka.Length < 5)
                            result = "Lozinka mora da ima bar 5 karaktera";
                        break;

                    case "TipKorisnika":
                        if (string.IsNullOrWhiteSpace(TipKorisnika.ToString()))
                            result = "Tip korisnika ne moze biti prazan!";
                        break;

                    case "JMBG":
                        if (string.IsNullOrWhiteSpace(JMBG))
                            result = "JMBG ne moze biti prazan!";
                        break;
                }
                if (ErrorCollection.ContainsKey(columnName))
                    ErrorCollection[columnName] = result;
                else if (result != null)
                    ErrorCollection.Add(columnName, result);

                OnPropertyChanged("ErrorCollection");

                return result;
            }
          }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
