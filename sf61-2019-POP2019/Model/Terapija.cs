﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public class Terapija : IDataErrorInfo,INotifyPropertyChanged, ICloneable
    {
        private int _id; 

        public int ID
        {
            get { return _id;  }
            set { _id = value; OnPropertyChanged("ID"); }
        }

        private string _opisTerapije;

        public string OpisTerapije
        {
            get { return _opisTerapije; }
            set { _opisTerapije = value; OnPropertyChanged("OpisTerapije"); }
        }

        private Pacijent _pacijent;
        public Pacijent Pacijent
        {
            get
            {
                return _pacijent;
            }
            set
            {
                _pacijent = value;OnPropertyChanged("Pacijent");
            }
        }

        private Lekar _ljekar;
        public Lekar Ljekar
        {
            get
            {
                return _ljekar;
            }
            set
            {
                _ljekar = value; OnPropertyChanged("Ljekar");
            }
        }

        private bool _aktivan;

        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        public string Error {
            get
            {
                return "poruka";
            }
        }
        public string this[string columnName] 
        { 
            get
            {
                switch(columnName)
                { 
                    case "OpisTerapije":
                        if (string.IsNullOrEmpty(OpisTerapije))
                            return "Opis terapije je obavezno polje!";
                        break;
                }
                return String.Empty;
            } 
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public object Clone()
        {
            Terapija kopija = new Terapija();
            kopija.ID = ID;
            kopija.Ljekar = Ljekar;
            kopija.OpisTerapije = OpisTerapije;
            kopija.Aktivan = Aktivan;

            return kopija;
        }

        public Terapija()
        {
            Ljekar = Ljekar;
            Aktivan = true;
        }

        public static void ObrisiTerapiju(Terapija selektovanaTerapija)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Terapije set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", selektovanaTerapija.ID));
                command.Parameters.Add(new SqlParameter("@OpisTerapije", selektovanaTerapija.OpisTerapije));
                command.Parameters.Add(new SqlParameter("@Aktivan", selektovanaTerapija.Aktivan));
                command.Parameters.Add(new SqlParameter("@IDLekara", selektovanaTerapija.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDPacijenta", selektovanaTerapija.Pacijent.ID));

                command.ExecuteNonQuery();
            }
            Util.Instance.UcitajTerapijeIzBaze();
        }

        public static void IzmijeniTerapiju(Terapija terapija)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Terapije set OpisTerapije=@OpisTerapije, Aktivan=@Aktivan, IDLekara=@IDLekara, IDPacijenta=@IDPacijenta where ID=@ID"; 

                command.Parameters.Add(new SqlParameter("@ID", terapija.ID));
                command.Parameters.Add(new SqlParameter("@OpisTerapije", terapija.OpisTerapije));
                command.Parameters.Add(new SqlParameter("@Aktivan", terapija.Aktivan));
                command.Parameters.Add(new SqlParameter("@IDLekara", terapija.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDPacijenta", terapija.Pacijent.ID));


                command.ExecuteNonQuery();
            }
            Util.Instance.UcitajTerapijeIzBaze();
        }

        public static void DodajTerapiju(Terapija terapija)
        {
            using(SqlConnection connection = new SqlConnection(Util.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "insert into Terapije(OpisTerapije,Aktivan,IDLekara, IDPacijenta) values " +
                    "(@OpisTerapije,@Aktivan,@IDLekara,@IDPacijenta)";
                command.Parameters.Add(new SqlParameter("@OpisTerapije", terapija.OpisTerapije));
                command.Parameters.Add(new SqlParameter("@Aktivan", terapija.Aktivan));
                command.Parameters.Add(new SqlParameter("@IDLekara", terapija.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDPacijenta", terapija.Pacijent.ID));


                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajTerapijeIzBaze();
        }
    }
}
