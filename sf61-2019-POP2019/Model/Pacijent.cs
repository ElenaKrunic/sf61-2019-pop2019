﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public class Pacijent : Korisnik, ICloneable, INotifyPropertyChanged, IDataErrorInfo
    {
        private Lekar ljekar;

        public Lekar Ljekar
        {
            get { return ljekar; }
            set {
                ljekar = value;
                OnPropertyChanged("Ljekar");
            }
        }


        private ObservableCollection<Terapija> listaTerapija = new ObservableCollection<Terapija>();
        public ObservableCollection<Terapija> ListaTerapija
        {
            get { return listaTerapija; }
            set
            {
                listaTerapija = value;
                OnPropertyChanged("ListaTerapija");
            }
        }

        private ObservableCollection<Termin> listaZakazanihTermina;

        public ObservableCollection<Termin> ListaZakazanihTermina
        {
            get { return listaZakazanihTermina; }
            set
            {
                listaZakazanihTermina = value;
                OnPropertyChanged("ListaZakazanihTermina");
            }
        }
        public Pacijent() : base()
        {
            TipKorisnika = ETipKorisnika.PACIJENT;
            Aktivan = true;
            this.listaTerapija = null;
        }

        public override object Clone()
        {
            Pacijent kopija = new Pacijent();
            kopija.Ime = Ime;
            kopija.Prezime = Prezime;
            kopija.KorisnickoIme = KorisnickoIme;
            kopija.JMBG = JMBG;
            kopija.Email = Email;
            kopija.Adresa = Adresa;
            kopija.Pol = Pol;
            kopija.TipKorisnika = TipKorisnika;
            kopija.ListaTerapija = ListaTerapija;

            return kopija;
        }

        public string toString()
        {
            return base.ToString();
        }

        public static void DodajPacijenta(Pacijent pacijent)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Pacijenti(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDLekara,IDAdrese)" +
                    "values (@Ime, @Prezime, @KorisnickoIme, @Email, @Lozinka, @Aktivan, @JMBG, @Pol, @TipKorisnika, @IDLekara,@IDAdrese)";

                command.Parameters.Add(new SqlParameter("@Ime", pacijent.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", pacijent.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", pacijent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", pacijent.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", pacijent.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", pacijent.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", pacijent.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", pacijent.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", pacijent.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDLekara", pacijent.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", pacijent.Adresa.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajPacijenteIzBaze();

        }

        public static void IzmijeniPacijenta(Pacijent pacijent)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Pacijenti set Ime=@Ime, Prezime=@Prezime, KorisnickoIme=@KorisnickoIme, Email=@Email, Lozinka=@Lozinka," +
                                        "Aktivan=@Aktivan, JMBG=@JMBG, Pol=@Pol, TipKorisnika=@TipKorisnika, IDLekara=@IDLekara,IDAdrese=@IDAdrese WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", pacijent.ID));
                command.Parameters.Add(new SqlParameter("@Ime", pacijent.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", pacijent.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", pacijent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", pacijent.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", pacijent.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", pacijent.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", pacijent.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", pacijent.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", pacijent.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDLekara", pacijent.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", pacijent.Adresa.ID));


                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajPacijenteIzBaze();
        }

        public static void ObrisiPacijenta(Pacijent selektovanPacijent)
        {
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Pacijenti set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", selektovanPacijent.ID));
                command.Parameters.Add(new SqlParameter("@Ime", selektovanPacijent.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", selektovanPacijent.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", selektovanPacijent.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", selektovanPacijent.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", selektovanPacijent.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", selektovanPacijent.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", selektovanPacijent.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", selektovanPacijent.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", selektovanPacijent.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDLekara", selektovanPacijent.Ljekar.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", selektovanPacijent.Adresa.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajPacijenteIzBaze();
        }
    }
}
