﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    public class DomZdravlja : IDataErrorInfo, INotifyPropertyChanged, ICloneable
    {
        public string Error { get { return null; } }

        public Dictionary<string, string> ErrorCollection { get; private set; } = new Dictionary<string, string>();

        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("ID"); }
        }

        private string _nazivInstitucije; 

        public string NazivInstitucije
        {
            get { return _nazivInstitucije; }
            set { _nazivInstitucije = value; OnPropertyChanged("NazivInstitucije"); }
        }

        private Adresa _adresa; 

        public Adresa Adresa
        {
            get { return _adresa; }
            set { _adresa = value; OnPropertyChanged("Adresa"); }
        }

        private List<Lekar> ljekari = new List<Lekar>(); 
        
        public List<Lekar> Ljekari
        {
            get { return ljekari; }
            set { ljekari = value;OnPropertyChanged("Ljekari"); }
        }
       
        private bool _aktivan;
        public bool Aktivan
        {
            get { return _aktivan; }
            set { _aktivan = value; }
        }

        private List<Lekar> zaposljeniLjekari = new List<Lekar>();
        public List<Lekar> ZaposleniLjekari
        {
            get { return zaposljeniLjekari; }
            set { zaposljeniLjekari = value;OnPropertyChanged("ZaposleniLjekari"); }
        }

        public string this[string columnName]
        { 
            get
            {
                string result = null;

                switch(columnName)
                {
                    case "NazivInstitucije":
                        if (string.IsNullOrEmpty(NazivInstitucije))
                            return "Naziv institucije je obavezno polje!";
                        break;
                }
                if (ErrorCollection.ContainsKey(columnName))
                    ErrorCollection[columnName] = result;
                else if (result != null)
                    ErrorCollection.Add(columnName, result);

                OnPropertyChanged("ErrorCollection");
                return result;
            }
        }

        public static void IzmijeniDomZdravlja(DomZdravlja domZdravlja)
        {
            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update DomZdravlja set NazivInstitucije=@NazivInstitucije, Aktivan=@Aktivan,IDAdrese=@IDAdrese where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", domZdravlja.ID));
                command.Parameters.Add(new SqlParameter("@NazivInstitucije", domZdravlja.NazivInstitucije));
                command.Parameters.Add(new SqlParameter("@Aktivan", domZdravlja.Aktivan));
                command.Parameters.Add(new SqlParameter("@IDAdrese", domZdravlja.Adresa.ID));


                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajDzIzBaze();
        }

        internal static void DodajDomZdravlja(DomZdravlja domZdravlja)
        {
            using(SqlConnection connection = new SqlConnection(Util.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "insert into DomZdravlja (NazivInstitucije, Aktivan,IDAdrese) values (@NazivInstitucije, @Aktivan,@IDAdrese)";

                command.Parameters.Add(new SqlParameter("@NazivInstitucije", domZdravlja.NazivInstitucije));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));
                command.Parameters.Add(new SqlParameter("@IDAdrese", domZdravlja.Adresa.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajDzIzBaze();
        }

        public override string ToString()
        {
            return NazivInstitucije;
        }

        public static void ObrisiDomZdravlja(DomZdravlja zaBrisanje)
        {
            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = "update DomZdravlja set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", zaBrisanje.ID));
                command.Parameters.Add(new SqlParameter("@NazivInstitucije", zaBrisanje.NazivInstitucije));
                command.Parameters.Add(new SqlParameter("@Aktivan", zaBrisanje.Aktivan));
                command.Parameters.Add(new SqlParameter("@IDAdrese", zaBrisanje.Adresa.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajDzIzBaze();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public object Clone()
        {
            DomZdravlja kopija = new DomZdravlja();
            kopija.ID = ID;
            kopija.NazivInstitucije = NazivInstitucije;
            kopija.Adresa = Adresa;
            kopija.Aktivan = Aktivan;

            return kopija;
        }
    }
}
