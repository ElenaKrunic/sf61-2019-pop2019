﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Model
{
    
    //2. Nije implementirana listaTermina

    public class Lekar : Korisnik, IDataErrorInfo,INotifyPropertyChanged, ICloneable
    {
        public Lekar() : base()
        {
            TipKorisnika = ETipKorisnika.LEKAR;
            Aktivan = true; 
        }

        private DomZdravlja domZdravlja;
        public DomZdravlja DomZdravlja
        {
            get { return domZdravlja; }
            set { domZdravlja = value; OnPropertyChanged("DomZdravlja"); }
        }

        public override string ToString()
        {
            //return base.ToString();
            return base.ToString();
        }
      
        public override object Clone()
        {
            Lekar kopija = new Lekar();
            kopija.Ime = Ime;
            kopija.Prezime = Prezime;
            kopija.KorisnickoIme = KorisnickoIme;
            kopija.Email = Email;
            kopija.DomZdravlja = DomZdravlja;
            kopija.Adresa = Adresa;
            kopija.Aktivan = Aktivan;
            kopija.JMBG = JMBG;
            kopija.Lozinka = Lozinka;
            kopija.Pol = Pol;
            kopija.TipKorisnika = TipKorisnika;

            return kopija;
        }

        public static void DodajLjekara(Lekar lekar)
        {
            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Lekari(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDDomaZdravlja,IDAdrese)" + 
                    "values (@Ime, @Prezime, @KorisnickoIme, @Email, @Lozinka, @Aktivan, @JMBG, @Pol, @TipKorisnika, @IDDomaZdravlja,@IDAdrese)";

                command.Parameters.Add(new SqlParameter("@Ime", lekar.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", lekar.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", lekar.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", lekar.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", lekar.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", lekar.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", lekar.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", lekar.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", lekar.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDDomaZdravlja", lekar.DomZdravlja.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", lekar.Adresa.ID));

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajLjekareIzBaze();
        }

        public static void IzmijeniLjekara(Lekar lekar)
        {
            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Lekari set Ime=@Ime, Prezime=@Prezime, KorisnickoIme=@KorisnickoIme, Email=@Email, Lozinka=@Lozinka," + 
                                        "Aktivan=@Aktivan, JMBG=@JMBG, Pol=@Pol, TipKorisnika=@TipKorisnika, IDDomaZdravlja=@IDDomaZdravlja,IDAdrese=@IDAdrese WHERE ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", lekar.ID));
                command.Parameters.Add(new SqlParameter("@Ime", lekar.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", lekar.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", lekar.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", lekar.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", lekar.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", lekar.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", lekar.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", lekar.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", lekar.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDDomaZdravlja", lekar.DomZdravlja.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", lekar.Adresa.ID));


                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajLjekareIzBaze();
        }

        public static void ObrisiLjekara(Lekar lekar)
        {
           using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Lekari set Aktivan=0 where ID=@ID";

                command.Parameters.Add(new SqlParameter("@ID", lekar.ID));
                command.Parameters.Add(new SqlParameter("@Ime", lekar.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", lekar.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", lekar.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Email", lekar.Email));
                command.Parameters.Add(new SqlParameter("@Lozinka", lekar.Lozinka));
                command.Parameters.Add(new SqlParameter("@Aktivan", lekar.Aktivan));
                command.Parameters.Add(new SqlParameter("@JMBG", lekar.JMBG));
                command.Parameters.Add(new SqlParameter("@Pol", lekar.Pol));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", lekar.TipKorisnika));
                command.Parameters.Add(new SqlParameter("@IDDomaZdravlja", lekar.DomZdravlja.ID));
                command.Parameters.Add(new SqlParameter("@IDAdrese", lekar.Adresa.ID));
             

                command.ExecuteNonQuery();
            }

            Util.Instance.UcitajLjekareIzBaze();
        }
    }
}
