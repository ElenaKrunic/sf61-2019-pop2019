﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Exceptions
{
    public class DomZdravljaNotFoundException : Exception
    {
        public DomZdravljaNotFoundException () : base()
        {

        }

        public DomZdravljaNotFoundException(string message) : base (message)
        {

        }

        public DomZdravljaNotFoundException(string message, Exception innerException) : base(message,innerException)
        {

        }
    }
}
