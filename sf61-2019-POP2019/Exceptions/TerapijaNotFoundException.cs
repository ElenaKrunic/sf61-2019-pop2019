﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Exceptions
{
    public class TerapijaNotFoundException : Exception
    {
        public TerapijaNotFoundException() : base()
        {

        }

        public TerapijaNotFoundException(string message) : base(message)
        {

        }

        public TerapijaNotFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
