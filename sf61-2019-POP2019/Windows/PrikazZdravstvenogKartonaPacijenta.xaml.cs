﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazZdravstvenogKartonaPacijenta.xaml
    /// </summary>
    public partial class PrikazZdravstvenogKartonaPacijenta : Window
    {
        Pacijent pacijent;
        ICollectionView view;
        ObservableCollection<Terapija> ListaTerapija;
        string searchFilter = "Active";

        public PrikazZdravstvenogKartonaPacijenta(Pacijent pacijent)
        {
            InitializeComponent();

            this.pacijent = pacijent;

            UpdateView(); 
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(VratiListuTerapija());
            dgZdravstveniKarton.ItemsSource = view;
            dgZdravstveniKarton.IsSynchronizedWithCurrentItem = true;
            dgZdravstveniKarton.IsReadOnly = true;
            dgZdravstveniKarton.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;

        }

        private bool CustomFilter(object obj)
        {
            Terapija terapija = obj as Terapija;
            if (searchFilter.Equals("SEARCH") && String.IsNullOrEmpty(txtOpisTerapije.Text))
            {
                return (terapija.Aktivan && terapija.OpisTerapije.ToLower().Contains(txtOpisTerapije.Text.ToLower()))
                       && (terapija.Aktivan && terapija.Ljekar.Ime.ToLower().Contains(txtLjekar.Text.ToLower()));
            }
            if (searchFilter.Equals("SEARCH"))
                return (terapija.Aktivan && terapija.OpisTerapije.ToLower().Contains(txtOpisTerapije.Text.ToLower()))
                    && (terapija.Aktivan && terapija.Ljekar.Ime.ToLower().Contains(txtLjekar.Text.ToLower()));
            return terapija.Aktivan;
        }

        
      private ObservableCollection<Terapija> VratiListuTerapija()
      {
          ListaTerapija = new ObservableCollection<Terapija>();

          foreach(var terapija in Util.Instance.Terapije)
          {
              if (pacijent.ID.Equals(terapija.Pacijent.ID))
              {
                  ListaTerapija.Add(terapija);
              }
          }

          return ListaTerapija;
      }
      

        private void dgZdravstveniKarton_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Pacijent"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtOpisTerapije_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtLjekar_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }
    }
}
