﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for NeprijavljenKorisnikWindow.xaml
    /// </summary>
    public partial class NeprijavljenKorisnikWindow : Window
    {
        ICollectionView view;
        DomZdravlja domZdravlja;
        string searchFilter = "Active";

        public NeprijavljenKorisnikWindow()
        {
            InitializeComponent();

            UpdateView();

        }
        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.DomoviZdravlja);
            dgDomoviZdravlja.IsSynchronizedWithCurrentItem = true;
            dgDomoviZdravlja.ItemsSource = view;
            dgDomoviZdravlja.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }

        private void btnVidiLjekare_Click(object sender, RoutedEventArgs e)
        {
            if(view.CurrentItem is DomZdravlja domZdravlja)
            {
                PrikazZaposleniLjekariWindow pzljw = new PrikazZaposleniLjekariWindow(domZdravlja);
                pzljw.Show();
            }
        }

        private void otkaziLjekare_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgDomoviZdravlja_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
               if(e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Ljekari") || e.PropertyName.Equals("ZaposleniLjekari"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }    
        }

     
        private bool CustomFilter(object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            if (searchFilter.Equals("SEARCH") && String.IsNullOrEmpty(TxtNazivDomaZdravlja.Text))
            {
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                       && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            }
            if (searchFilter.Equals("SEARCH"))
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                    && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            return domZdravlja.Aktivan;
        }
        private void txtUlica_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }

        private void TxtNazivDomaZdravlja_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
