﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazTerminaZaZakazivanjeKodPacijenta.xaml
    /// </summary>
    public partial class PrikazTerminaZaZakazivanjeKodPacijenta : Window
    {
        ICollectionView view; 

        public PrikazTerminaZaZakazivanjeKodPacijenta(Pacijent pacijent)
        {
            InitializeComponent();

            UpdateView();
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Termini);
            dgPrikazSvihTerminaLjekara.ItemsSource = view;
            dgPrikazSvihTerminaLjekara.IsSynchronizedWithCurrentItem = true;
            dgPrikazSvihTerminaLjekara.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
        }

        private void dgPrikazSvihTerminaLjekara_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("ID") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Aktivan"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnZakaziPregled_Click(object sender, RoutedEventArgs e)
        {

            Termin selektovanTermin = view.CurrentItem as Termin;

            if (selektovanTermin != null)
            {
                Termin stariTermin = selektovanTermin.Clone() as Termin;

                DodajIzmijeniTermin izmijeniTermin = new DodajIzmijeniTermin(selektovanTermin, DodajIzmijeniTermin.Opcija.IZMIJENI);
                if (izmijeniTermin.ShowDialog() != true)
                {
                    int index = Util.Instance.Termini.IndexOf(selektovanTermin);
                    Util.Instance.Termini[index] = stariTermin;
                }
            }
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
