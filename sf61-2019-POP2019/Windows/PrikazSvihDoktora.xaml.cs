﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihDoktora.xaml
    /// </summary>
    public partial class PrikazSvihDoktora : Window
    {
        ICollectionView view;
        string searchFilter = "Active";
        public PrikazSvihDoktora()
        {
            InitializeComponent();

            cmbTipKorisnika.ItemsSource = new List<ETipKorisnika> { ETipKorisnika.ADMINISTRATOR, ETipKorisnika.LEKAR, ETipKorisnika.PACIJENT };
            DGLekari.IsReadOnly = true;

            UpdateView();

        }
        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Lekari);
            DGLekari.ItemsSource = view;
            DGLekari.IsSynchronizedWithCurrentItem = true;
           // DGLekari.IsReadOnly = true;
            //prosiruje na sirinu data grida 
            DGLekari.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            if (searchFilter.Equals("SEARCH") && cmbTipKorisnika.SelectedIndex > -1)
                return ((korisnik.Aktivan &&
                        korisnik.Prezime.ToLower().Contains(txtPrezime.Text.ToLower()) &&
                        korisnik.Ime.ToLower().Contains(txtIme.Text.ToLower()) &&
                        korisnik.Email.ToLower().Contains(txtEmail.Text.ToLower()) &&
                        korisnik.Adresa.Ulica.ToLower().Contains(txtAdresa.Text.ToLower()) &&
                        korisnik.TipKorisnika.ToString().ToLower().Equals(cmbTipKorisnika.SelectedItem.ToString().ToLower())));
            else if (searchFilter.Equals("SEARCH"))
                return ((korisnik.Aktivan &&
                        korisnik.Ime.ToLower().Contains(txtIme.Text.ToLower()) &&
                 korisnik.Prezime.ToLower().Contains(txtPrezime.Text.ToLower()) &&
                 korisnik.Email.ToLower().Contains(txtEmail.Text.ToLower())));
            else
                return korisnik.Aktivan;
        }

        private void DGLekari_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if(e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Error"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }    
        }

        private void BtnDodajDoktora_Click(object sender, RoutedEventArgs e)
        {
            Lekar lekar = new Lekar();
            DodajIzmijeniLjekara dodajLekara = new DodajIzmijeniLjekara(lekar, DodajIzmijeniLjekara.Opcija.DODAJ);
            dodajLekara.ShowDialog();
        }

        private void BtnIzmijeniDoktora_Click(object sender, RoutedEventArgs e)
        {
            Lekar selektovanLjekar = view.CurrentItem as Lekar;
            if(selektovanLjekar != null)
            {
                Lekar stariLekar = selektovanLjekar.Clone() as Lekar;

                DodajIzmijeniLjekara izmijeniLjekara = new DodajIzmijeniLjekara(selektovanLjekar, DodajIzmijeniLjekara.Opcija.IZMIJENI);
                if(izmijeniLjekara.ShowDialog() != true)
                {
                    int index = Util.Instance.Lekari.IndexOf(selektovanLjekar);
                    Util.Instance.Lekari[index] = stariLekar;
                }
            }
        
        }
        private void btnObrisiDoktora_Click(object sender, RoutedEventArgs e)
        {

            if(view.CurrentItem is Lekar selektovanLjekar)
            {
                if(MessageBox.Show("Da li ste sigurni? ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Lekar.ObrisiLjekara(selektovanLjekar);
                }
            }
        }

        private void TxtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            //vraca rezultat
            this.Close();
           
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }

        private void txtPrezime_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtEmail_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void cmbTipKorisnika_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtAdresa_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
