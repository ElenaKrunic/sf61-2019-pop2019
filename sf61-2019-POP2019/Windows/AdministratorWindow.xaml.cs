﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for AdministratorWindow.xaml
    /// </summary>
    public partial class AdministratorWindow : Window
    {
        Administrator admin;
        public AdministratorWindow(Administrator admin)
        {
            InitializeComponent();
            this.admin = admin;

            this.DataContext = admin;
            txtIme.DataContext = admin;
            txtPrezime.DataContext = admin;
            txtKorisnickoIme.DataContext = admin;
            txtEmail.DataContext = admin;
            txtJMBG.DataContext = admin;
            txtLozinka.DataContext = admin;
            txtTipKorisnika.DataContext = admin;
            txtPol.DataContext = admin;
        }

        private void btnLjekari_Click(object sender, RoutedEventArgs e)
        {
            PrikazSvihDoktora ljekari = new PrikazSvihDoktora();
            ljekari.ShowDialog();
        }

        private void btnPacijenti_Click(object sender, RoutedEventArgs e)
        {
            PrikazSvihPacijenata pacijenti = new PrikazSvihPacijenata();
            pacijenti.ShowDialog();
        }

        private void btnDomoviZdravlja_Click(object sender, RoutedEventArgs e)
        {
            PrikazSvihDomovaZdravlja domoviZdravlja = new PrikazSvihDomovaZdravlja();
            domoviZdravlja.ShowDialog();
        }

        private void btnTerapije_Click(object sender, RoutedEventArgs e)
        {
            PrikazSvihTerapija terapije = new PrikazSvihTerapija();
            terapije.ShowDialog();
        }

        private void btnTermini_Click(object sender, RoutedEventArgs e)
        {
            PrikazSvihTermina termini = new PrikazSvihTermina();
            termini.ShowDialog();

        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li stvarno zelite da se odjavite?", "Confirm", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
            {
                HomeWindow homeWindow = new HomeWindow();
                homeWindow.ShowDialog();
            }
        }

        private void btnSacuvajIzmjene_Click(object sender, RoutedEventArgs e)
        {
            if(admin.ID != 0)
            {
                Administrator.IzmijeniAdministrator(admin);
                MessageBox.Show("Uspjesno odradjena izmjena podataka! ", "Izmjena podataka", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }
    }
}
