﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PacijentWindow.xaml
    /// </summary>
    public partial class PacijentWindow : Window
    {
        //ICollectionView viewListaTerapija;
        //ICollectionView viewListaDomovaZdravlja; 
        //ObservableCollection<Terapija> ListaTerapija;
        //ObservableCollection<DomZdravlja> ListaDomovaZdravlja; 
        Pacijent pacijent;
        ICollectionView view;
        
        public PacijentWindow(Pacijent pacijent)
        {
            InitializeComponent();

            this.pacijent = pacijent;

            txtIme.DataContext = pacijent;
            txtPrezime.DataContext = pacijent;
            txtLozinka.DataContext = pacijent;
            txtKorisnickoIme.DataContext = pacijent;
            txtJMBG.DataContext = pacijent;
            txtEmail.DataContext = pacijent;
            txtTipKorisnika.DataContext = pacijent;
            txtPol.DataContext = pacijent;

         
           // UpdateViewWithZdravstveniKarton();

           // UpdateViewWithDomoviZdravlja();
        }

        /*
        private void UpdateViewWithDomoviZdravlja()
        {
           
        }
        */

        /*
        private void UpdateViewWithZdravstveniKarton()
        {
            viewListaTerapija = CollectionViewSource.GetDefaultView(VratiListuTerapija());
            dgZdravstveniKarton.ItemsSource = viewListaTerapija;
            dgZdravstveniKarton.IsSynchronizedWithCurrentItem = true;
            dgZdravstveniKarton.IsReadOnly = true;
            dgZdravstveniKarton.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
        }
        */

        private void btnIzmjena_Click(object sender, RoutedEventArgs e)
        { 
            if(pacijent.ID != 0)
            {
                Pacijent.IzmijeniPacijenta(pacijent);
                MessageBox.Show("Uspjesno odradjena izmjena podataka! ", "Izmjena podataka", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            HomeWindow hw = new HomeWindow();
            hw.ShowDialog();
        }

        private void dgZdravstveniKarton_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Ljekar") || e.PropertyName.Equals("Pacijent"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnZdravstveniKarton_Click(object sender, RoutedEventArgs e)
        {
            PrikazZdravstvenogKartonaPacijenta pzkp = new PrikazZdravstvenogKartonaPacijenta(pacijent);
            pzkp.ShowDialog(); 
        }

        private void btnDomoviZdravlja_Click(object sender, RoutedEventArgs e)
        {
           PrikazDomovaZdravljaPacijenta pdzp = new PrikazDomovaZdravljaPacijenta(pacijent);
           pdzp.ShowDialog();
        }

        private void btnZakazivanjeTermina_Click(object sender, RoutedEventArgs e)
        {
            PrikazTerminaZaZakazivanjeKodPacijenta ptzzkp = new PrikazTerminaZaZakazivanjeKodPacijenta(pacijent);
             ptzzkp.ShowDialog();
            // DodajIzmijeniTermin izmijeniTermin = DodajIzmijeniTermin()
        }
    }
 }

