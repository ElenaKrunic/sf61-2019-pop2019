﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihAdministratora.xaml
    /// </summary>
    public partial class PrikazSvihAdministratora : Window
    {
        ICollectionView view;
        public PrikazSvihAdministratora()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Util.Instance.Administratori);
            DGAdministratori.ItemsSource = view;

            DGAdministratori.IsSynchronizedWithCurrentItem = true;
            DGAdministratori.IsReadOnly = true;

            DGAdministratori.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }


        private void DGAdministratori_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Adresa"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void BtnDodajAdmina_Click(object sender, RoutedEventArgs e)
        {
            Administrator administrator = new Administrator();
            DodajIzmijeniAdministratora dodajAdmina = new DodajIzmijeniAdministratora(administrator, DodajIzmijeniAdministratora.Opcija.DODAJ);
            dodajAdmina.ShowDialog();
        }

        private void BtnIzmijeniAdmina_Click(object sender, RoutedEventArgs e)
        {
            Administrator selectedAdmin = view.CurrentItem as Administrator;
            if (selectedAdmin != null)
            {
                Administrator oldAdmin = selectedAdmin.Clone() as Administrator;

                DodajIzmijeniAdministratora adminEditWindow = new DodajIzmijeniAdministratora(selectedAdmin, DodajIzmijeniAdministratora.Opcija.IZMIJENI);
                if (adminEditWindow.ShowDialog() != true)
                {
                    int index = Util.Instance.Administratori.IndexOf(selectedAdmin);
                    Util.Instance.Administratori[index] = oldAdmin;
                }
            }
        }

        private void btnObrisiAdmina_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentItem is Administrator selectedAdmin)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Administrator.ObrisiAdministratora(selectedAdmin);
                }
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
          
        }
    }
}
