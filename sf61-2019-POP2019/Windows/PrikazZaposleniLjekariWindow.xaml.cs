﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazZaposleniLjekariWindow.xaml
    /// </summary>
    public partial class PrikazZaposleniLjekariWindow : Window
    {
        ICollectionView view;
        DomZdravlja domZdravlja;
        public PrikazZaposleniLjekariWindow(DomZdravlja domZdravlja)
        {
            ObservableCollection<Lekar> ljekari = new ObservableCollection<Lekar>();
            InitializeComponent();
            this.domZdravlja = domZdravlja;

            foreach(var lj in Util.Instance.Lekari)
            {
                if(lj.DomZdravlja.ID.Equals(domZdravlja.ID))
                {
                    ljekari.Add(lj);
                }
            }

            view = CollectionViewSource.GetDefaultView(ljekari);
            dgZaposleniLjekari.ItemsSource = view;

            dgZaposleniLjekari.IsSynchronizedWithCurrentItem = true;
            dgZaposleniLjekari.IsReadOnly = true;

            dgZaposleniLjekari.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            //UpdateView();
            /*
            //ObservableCollection<Lekar> ljekari = new ObservableCollection<Lekar>();
            ObservableCollection<Korisnik> ljekari = new ObservableCollection<Korisnik>();

            InitializeComponent();

            this.domZdravlja = domZdravlja;

           foreach(var ljekar in Util.Instance.Korisnici)
            {
                if(ljekar is Lekar)
                {
                    var l = ljekar as Lekar;
                    if (l.DomZdravlja.ID.Equals(domZdravlja.ID))
                    {
                        ljekari.Add(l);
                    }
                }
            }

            view = CollectionViewSource.GetDefaultView(ljekari);
            dgZaposleniLjekari.ItemsSource = view;

            dgZaposleniLjekari.IsSynchronizedWithCurrentItem = true;
            dgZaposleniLjekari.IsReadOnly = true;
            dgZaposleniLjekari.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            */
        }

        public void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(domZdravlja.ZaposleniLjekari);
            dgZaposleniLjekari.ItemsSource = view;
        }

        private void dgZaposleniLjekari_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Ljekari") || e.PropertyName.Equals("ZaposleniLjekari"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
