﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazTerminaKodLjekara.xaml
    /// </summary>
    public partial class PrikazTerminaKodLjekara : Window
    {
        ICollectionView view;
        Lekar lekar;
        ObservableCollection<Termin> Termini;
        public PrikazTerminaKodLjekara(Lekar lekar)
        {
            InitializeComponent();

            this.lekar = lekar; 

            UpdateView(); 
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(ReturnListaTermina());
            dgTerminiKodLjekara.ItemsSource = view;
            dgTerminiKodLjekara.IsSynchronizedWithCurrentItem = true;
            dgTerminiKodLjekara.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            
        }

        private ObservableCollection<Termin> ReturnListaTermina()
        {
            Termini = new ObservableCollection<Termin>();

            foreach (var t in Util.Instance.Termini)
            {
                if (t.LjekarTermin.ID.Equals(lekar.ID))
                {
                    Termini.Add(t);
                }
            }

            return Termini;
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgTerminiKodLjekara_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("PacijentTermin") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("LjekarTermin"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }
    }
}
