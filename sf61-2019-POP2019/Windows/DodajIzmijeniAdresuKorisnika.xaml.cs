﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniAdresuKorisnika.xaml
    /// </summary>
    public partial class DodajIzmijeniAdresuKorisnika : Window
    {
        public enum Opcija { DODAJ,IZMIJENI };
        Opcija opcija;
        public Adresa adresa;
        public DodajIzmijeniAdresuKorisnika(Adresa poljaAdrese, Opcija opcija)
        {
            this.opcija = opcija; 
            InitializeComponent();

            adresa = new Adresa(); 

            if(opcija == Opcija.IZMIJENI)
            {
                adresa = poljaAdrese;
            }
            
            TxtID.DataContext = adresa;
            TxtBroj.DataContext = adresa;
            TxtDrzava.DataContext = adresa;
            TxtGrad.DataContext = adresa;
            TxtUlica.DataContext = adresa;  
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {

            if(opcija == Opcija.DODAJ)
            {
                Adresa.DodajAdresu(adresa);
                Util.Instance.Adrese.Add(adresa);
                this.DialogResult = true;
            }
            else
            {
                adresa.Aktivan = true;
                Adresa.IzmijeniAdresu(adresa);
                this.DialogResult = true; 
            }

            this.Close();
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
