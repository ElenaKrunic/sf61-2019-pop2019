﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniDomZdravlja.xaml
    /// </summary>
    public partial class DodajIzmijeniDomZdravlja : Window
    {
        public enum Opcija { DODAJ,IZMIJENI};
        Opcija opcija;
        private DomZdravlja domZdravlja; 
        public DodajIzmijeniDomZdravlja(DomZdravlja domZdravlja, Opcija opcija)
        {
            InitializeComponent();

            this.DataContext = domZdravlja;

            this.domZdravlja = domZdravlja;
            this.opcija = opcija;

            TxtNaziv.DataContext = domZdravlja;
            BtnAdresa.DataContext = domZdravlja;
            BtnAdresa.Content = "Dodaj adresu"; 

            if(opcija.Equals(Opcija.IZMIJENI))
            {
                BtnAdresa.Content = "Izmijeni adresu";
            }
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            if(domZdravlja.ID != 0)
            {
                DomZdravlja.IzmijeniDomZdravlja(domZdravlja);
            }

            else
            {
                Util.Instance.DomoviZdravlja.Add(domZdravlja);
                DomZdravlja.DodajDomZdravlja(domZdravlja);
            }

            this.Close();
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            //vraca rezultat
            this.DialogResult = false;
            this.Close();
        }
        private void BtnAdresa_Click(object sender, RoutedEventArgs e)
        {

            Adresa poljaAdrese = domZdravlja.Adresa;

            if (opcija == Opcija.DODAJ)
            {
                DodajIzmijeniAdresuKorisnika w = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.DODAJ);

                if (w.ShowDialog() == true)
                {
                    domZdravlja.Adresa = w.adresa;
                }
            }
            else
            {
                DodajIzmijeniAdresuKorisnika izmijeni = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.IZMIJENI);
                izmijeni.ShowDialog();
            }
            
        }
    }
}
