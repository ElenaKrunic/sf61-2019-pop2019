﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazDomovaZdravljaLjekar.xaml
    /// </summary>
    public partial class PrikazDomovaZdravljaLjekar : Window
    {
        Lekar ljekar;
        ICollectionView view;
        string searchFilter = "Active";
        public PrikazDomovaZdravljaLjekar(Lekar ljekar)
        {
            InitializeComponent();

            this.ljekar = ljekar;

            UpdateView(); 
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.DomoviZdravlja);
            dgDomoviZdravlja.ItemsSource = view;
            dgDomoviZdravlja.IsSynchronizedWithCurrentItem = true;
            dgDomoviZdravlja.IsReadOnly = true;
            dgDomoviZdravlja.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            if (searchFilter.Equals("SEARCH") && String.IsNullOrEmpty(TxtNazivDomaZdravlja.Text))
            {
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                       && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            }
            if (searchFilter.Equals("SEARCH"))
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                    && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            return domZdravlja.Aktivan;
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPretazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }

        private void dgDomoviZdravlja_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("Ljekari") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("ZaposleniLjekari"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void TxtNazivDomaZdravlja_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtUlica_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
