﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for LjekarWindow.xaml
    /// </summary>
    public partial class LjekarWindow : Window
    {
        ICollectionView viewZdravstevniKartoni;
        ObservableCollection<Terapija> Terapije; 
        Lekar lekar;
        public LjekarWindow(Lekar lekar)
        {
            InitializeComponent();

            this.lekar = lekar;

            foreach(var dz in Util.Instance.DomoviZdravlja)
            {
                cmbBoxDomZdravlja.Items.Add(dz);
            }

            this.DataContext = lekar;
            txtIme.DataContext = lekar;
            txtPrezime.DataContext = lekar;
            txtEmail.DataContext = lekar;
            txtJMBG.DataContext = lekar;
            txtKorisnickoIme.DataContext = lekar;
            txtLozinka.DataContext = lekar;
            txtTipKorisnika.DataContext = lekar;
            txtPol.DataContext = lekar;
            cmbBoxDomZdravlja.DataContext = lekar;

            //UpdateViewWithTermini();

            UpdateViewWithZdravstveniKartoni();
        }

        
        private void UpdateViewWithZdravstveniKartoni()
        {
            viewZdravstevniKartoni = CollectionViewSource.GetDefaultView(ReturnZdravstveniKartoni());
            dgPrikazZdravstvenihKartonaPacijenata.ItemsSource = viewZdravstevniKartoni;
            dgPrikazZdravstvenihKartonaPacijenata.IsSynchronizedWithCurrentItem = true;
            dgPrikazZdravstvenihKartonaPacijenata.IsReadOnly = true;
            dgPrikazZdravstvenihKartonaPacijenata.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
        }
        
        
        private ObservableCollection<Terapija> ReturnZdravstveniKartoni()
        {
            Terapije = new ObservableCollection<Terapija>(); 

            foreach(var te in Util.Instance.Terapije)
            {
                if(te.Ljekar.ID.Equals(lekar.ID))
                {
                    Terapije.Add(te);
                }
            }

            return Terapije;
        }

       
        private void btnSacuvajIzmjene_Click(object sender, RoutedEventArgs e)
        {
            if(lekar.ID != 0)
            {
                Lekar.IzmijeniLjekara(lekar);
                MessageBox.Show("Uspjesno odradjena izmjena podataka! ", "Izmjena podataka", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            HomeWindow hw = new HomeWindow();
            hw.ShowDialog();
        }

        private void dgPrikazZdravstvenihKartonaPacijenata_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Pacijent"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }
        
        private void btnDodajTerapijuPacijentu_Click(object sender, RoutedEventArgs e)
        {
            Terapija terapija = new Terapija();
            DodajIzmijeniTerapiju dodajTerapiju = new DodajIzmijeniTerapiju(terapija, DodajIzmijeniTerapiju.Opcija.DODAJ);
            dodajTerapiju.ShowDialog();
        }

        
        private void btnVidiTermine_Click(object sender, RoutedEventArgs e)
        {
            PrikazTerminaKodLjekara ptklj = new PrikazTerminaKodLjekara(lekar);
            ptklj.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrikazDomovaZdravljaLjekar pdzlj = new PrikazDomovaZdravljaLjekar(lekar);
            pdzlj.ShowDialog();
        }
    }
}
