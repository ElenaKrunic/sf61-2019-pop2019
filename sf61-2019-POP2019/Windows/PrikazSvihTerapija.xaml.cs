﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihTerapija.xaml
    /// </summary>
    public partial class PrikazSvihTerapija : Window
    {
        ICollectionView view;

        string searchFilter = "Active";
        public PrikazSvihTerapija()
        {
            InitializeComponent();

            UpdateView();
        }

        private bool CustomFilter(object obj)
        {
            Terapija terapija = obj as Terapija;
            if (searchFilter.Equals("SEARCH") && String.IsNullOrEmpty(txtOpisTerapije.Text))
            {
                return (terapija.Aktivan && terapija.OpisTerapije.ToLower().Contains(txtOpisTerapije.Text.ToLower()))
                       && (terapija.Aktivan && terapija.Ljekar.Ime.ToLower().Contains(txtLjekar.Text.ToLower()));
            }
            if (searchFilter.Equals("SEARCH"))
                return (terapija.Aktivan && terapija.OpisTerapije.ToLower().Contains(txtOpisTerapije.Text.ToLower()))
                    && (terapija.Aktivan && terapija.Ljekar.Ime.ToLower().Contains(txtLjekar.Text.ToLower()));
            return terapija.Aktivan;
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Terapije);
            DGTerapije.IsSynchronizedWithCurrentItem = true;
            DGTerapije.ItemsSource = view;

            DGTerapije.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }


        private void DGTerapije_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void BtnDodajTerapiju_Click(object sender, RoutedEventArgs e)
        {
            Terapija terapija = new Terapija();
            DodajIzmijeniTerapiju dodajTerapiju = new DodajIzmijeniTerapiju(terapija, DodajIzmijeniTerapiju.Opcija.DODAJ);
            dodajTerapiju.ShowDialog();
        }

        private void BtnIzmijeniTerapiju_Click(object sender, RoutedEventArgs e)
        {
            Terapija selektovanaTerapija = view.CurrentItem as Terapija; 

            if(selektovanaTerapija != null)
            {
                Terapija staraTerapija = selektovanaTerapija.Clone() as Terapija;

                DodajIzmijeniTerapiju izmijeniTerapiju = new DodajIzmijeniTerapiju(selektovanaTerapija, DodajIzmijeniTerapiju.Opcija.IZMIJENI); 

                if(izmijeniTerapiju.ShowDialog() != true)
                {
                    int index = Util.Instance.Terapije.IndexOf(selektovanaTerapija);

                    Util.Instance.Terapije[index] = staraTerapija;
                }

            }
        }

        private void BtnObrisiTerapiju_Click(object sender, RoutedEventArgs e)
        {
            Terapija selektovanaTerapija = view.CurrentItem as Terapija; 

            if(selektovanaTerapija != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Confirm", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Terapija.ObrisiTerapiju(selektovanaTerapija);
                }
            }
        }

        private void TxtBoxSifraTerapije_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
         
        }

        private void txtOpisTerapije_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtLjekar_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }
    }
}
