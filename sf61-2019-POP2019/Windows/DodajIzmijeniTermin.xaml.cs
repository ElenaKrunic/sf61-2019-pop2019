﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniTermin.xaml
    /// </summary>
    public partial class DodajIzmijeniTermin : Window
    {
        public enum Opcija { DODAJ, IZMIJENI};
        Opcija opcija;
        Termin termin;
        public DodajIzmijeniTermin(Termin termin, Opcija opcija)
        {
            InitializeComponent();

            this.DataContext = termin;
            this.termin = termin;
            this.opcija = opcija;
            txtDatumTermina.DataContext = termin;
            
            // CmbBoxPolKorisnika.Items.Add(EPol.Zensko);
            // CmbBoxPolKorisnika.Items.Add(EPol.Musko);

            cmbBoxStatusTermin.Items.Add(EStatusTermina.SLOBODAN);
            cmbBoxStatusTermin.Items.Add(EStatusTermina.ZAKAZAN);

            foreach (var dz in Util.Instance.DomoviZdravlja)
            {
                CmbBoxDomZdravlja.Items.Add(dz);
            }

            foreach(var l in Util.Instance.Lekari)
            {
                cmbBoxLjekar.Items.Add(l);
            }

            foreach(var p in Util.Instance.Pacijenti)
            {
                cmbBoxPacijent.Items.Add(p);
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true; 

            if(termin.ID != 0)
            {
                termin.Aktivan = true;
                Termin.IzmijeniTermin(termin);
            } else
            {
                termin.Aktivan = true;
                Util.Instance.Termini.Add(termin);
                Termin.DodajTermin(termin);
            }

            Close();
        }

        private void btnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
