﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniTerapiju.xaml
    /// </summary>
    public partial class DodajIzmijeniTerapiju : Window
    {
        public enum Opcija { DODAJ, IZMIJENI };
        private Terapija terapija;
        Opcija opcija;

        public DodajIzmijeniTerapiju(Terapija terapija, Opcija opcija)
        {
            InitializeComponent();

            this.DataContext = terapija;

            this.terapija = terapija;
            this.opcija = opcija;

            TxtOpisTerapije.DataContext = terapija; 
            
            foreach(var ljekar in Util.Instance.Lekari)
            {
                CmbBoxLjekarKojiJePrepisaoTerapiju.Items.Add(ljekar);
            }

            foreach(var pacijent in Util.Instance.Pacijenti)
            {
                CmbBoxPacijentKojemJePrepisanaTerapija.Items.Add(pacijent);
            }

            CmbBoxPacijentKojemJePrepisanaTerapija.DataContext = terapija;
            CmbBoxLjekarKojiJePrepisaoTerapiju.DataContext = terapija;
    
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true; 

            if(terapija.ID != 0)
            {
                Terapija.IzmijeniTerapiju(terapija); 
            }
            else
            {
                Util.Instance.Terapije.Add(terapija);
                Terapija.DodajTerapiju(terapija);
            }
        }

        private void BtnOtkaziTerapiju_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
