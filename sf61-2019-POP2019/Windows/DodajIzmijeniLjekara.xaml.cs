﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniLjekara.xaml
    /// </summary>
    public partial class DodajIzmijeniLjekara : Window
    { 
        public enum Opcija { DODAJ, IZMIJENI};
        Opcija opcija;
        Lekar lekar; 
        public DodajIzmijeniLjekara(Lekar lekar, Opcija opcija)
        {
            InitializeComponent();
            this.DataContext = lekar;
            this.lekar = lekar;
            this.opcija = opcija;

            SetButton();

            TxtName.DataContext = lekar;
            TxtPrezime.DataContext = lekar;
            TxtKorisnickoIme.DataContext = lekar;
            TxtEmail.DataContext = lekar;
            TxtLozinka.DataContext = lekar;
            TxtJMBG.DataContext = lekar;
            BtnAdresa.Content = "Dodaj adresu";
            BtnAdresa.DataContext = lekar;

            CmbBoxPolKorisnika.Items.Add(EPol.Zensko);
            CmbBoxPolKorisnika.Items.Add(EPol.Musko);

            foreach (var dz in Util.Instance.DomoviZdravlja)
            {
                CmbBoxDomZdravlja.Items.Add(dz);
            }

            CmbBoxPolKorisnika.DataContext = lekar;
            CmbBoxDomZdravlja.DataContext = lekar;

            if(opcija.Equals(Opcija.IZMIJENI))
            {
                TxtKorisnickoIme.IsEnabled = true;
                CmbBoxDomZdravlja.IsEnabled = true;
                TxtJMBG.IsEnabled = false;
                BtnAdresa.Content = "Izmijeni adresu";
            }

        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            if (lekar.ID != 0)
            {
                
                Lekar.IzmijeniLjekara(lekar);
                
            } else
            {
                Util.Instance.Lekari.Add(lekar);
                Lekar.DodajLjekara(lekar);
            }

            Close();

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnAdresa_Click(object sender, RoutedEventArgs e)
        {
            Adresa poljaAdrese = lekar.Adresa;

            if (opcija == Opcija.DODAJ)
            {
                DodajIzmijeniAdresuKorisnika w = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.DODAJ);

                if (w.ShowDialog() == true)
                {
                    lekar.Adresa = w.adresa;
                }
            }
            else
            {
                DodajIzmijeniAdresuKorisnika izmijeni = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.IZMIJENI);
                izmijeni.ShowDialog();
            }
        }

        private void TxtPrezime_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void TxtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void TxtKorisnickoIme_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void TxtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void TxtLozinka_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void TxtJMBG_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButton();
        }

        private void CmbBoxDomZdravlja_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetButton();
        }

        private void SetButton()
        {
            if ((TxtName.Text != string.Empty) && (TxtPrezime.Text != string.Empty) &&
                (TxtEmail.Text != string.Empty) && (TxtLozinka.Text != string.Empty)
                && (TxtKorisnickoIme.Text != string.Empty))
            {
                BtnOk.IsEnabled = true;
            }
            else
            {
                BtnOk.IsEnabled = false;
            }
        }

        private void CmbBoxPolKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetButton();
        }
    } 
}
