﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihTermina.xaml
    /// </summary>
    public partial class PrikazSvihTermina : Window
    {
        ICollectionView view;
        public PrikazSvihTermina()
        {
            InitializeComponent();
            UpdateView();
        }
        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Termini);
            dgTermini.ItemsSource = view;
            dgTermini.IsSynchronizedWithCurrentItem = true;
            dgTermini.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
        }

        private void dgTermini_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("ID") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("ErrorCollection"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnDodajTermin_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = new Termin();
            DodajIzmijeniTermin dodajTermin = new DodajIzmijeniTermin(termin, DodajIzmijeniTermin.Opcija.DODAJ);
            dodajTermin.ShowDialog();
        }

        private void btnIzmijeniTermin_Click(object sender, RoutedEventArgs e)
        {
            Termin selektovanTermin = view.CurrentItem as Termin;
            if (selektovanTermin != null)
            {
                Termin stariTermin = selektovanTermin.Clone() as Termin;

                DodajIzmijeniTermin izmijeniTermin = new DodajIzmijeniTermin(selektovanTermin, DodajIzmijeniTermin.Opcija.IZMIJENI);
                if (izmijeniTermin.ShowDialog() != true)
                {
                    int index = Util.Instance.Termini.IndexOf(selektovanTermin);
                    Util.Instance.Termini[index] = stariTermin;
                }
            }
        }

        private void btnObrisiTermin_Click(object sender, RoutedEventArgs e)
        {
            if (view.CurrentItem is Termin selektovanTermin)
            {
                if (MessageBox.Show("Da li ste sigurni? ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Termin.ObrisiTermin(selektovanTermin);
                }
            }
        }

        private void btnOtkaziTermin_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
