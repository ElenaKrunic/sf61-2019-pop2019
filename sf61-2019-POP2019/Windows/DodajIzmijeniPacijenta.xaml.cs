﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniPacijenta.xaml
    /// </summary>
    public partial class DodajIzmijeniPacijenta : Window
    {
        public enum Opcija { DODAJ,IZMIJENI};
        private Pacijent pacijent;
        Opcija opcija;

        public DodajIzmijeniPacijenta(Pacijent pacijent, Opcija opcija)
        {
            InitializeComponent();

            this.DataContext = pacijent;

            this.pacijent = pacijent;
            this.opcija = opcija;

            TxtImePacijenta.DataContext = pacijent;
            TxtPrezimePacijenta.DataContext = pacijent;
            TxtEmailPacijenta.DataContext = pacijent;
            TxtKorisnickoImePacijenta.DataContext = pacijent;
            TxtLozinka.DataContext = pacijent;
            TxtJMBG.DataContext = pacijent;
            BtnAdresa.Content = "Dodaj adresu";
            BtnAdresa.DataContext = pacijent;

            foreach (var ljekar in Util.Instance.Lekari)
            {
                CmbBoxLjekar.Items.Add(ljekar);
            }

            CmbBoxPolKorisnika.Items.Add(EPol.Zensko);
            CmbBoxPolKorisnika.Items.Add(EPol.Musko);

            CmbBoxPolKorisnika.DataContext = pacijent;
            CmbBoxLjekar.DataContext = pacijent;

            if (opcija.Equals(Opcija.IZMIJENI))
            {
                BtnAdresa.Content = "Izmijeni adresu";
                TxtJMBG.IsEnabled = false;
            }
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

            if (pacijent.ID != 0)
            {
                Pacijent.IzmijeniPacijenta(pacijent);
            }
            else
            {
                Util.Instance.Pacijenti.Add(pacijent);
                Pacijent.DodajPacijenta(pacijent);
            }

            Close();
        }


        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void BtnAdresa_Click(object sender, RoutedEventArgs e)
        {
            Adresa poljaAdrese = pacijent.Adresa;

            if (opcija == Opcija.DODAJ)
            {
                DodajIzmijeniAdresuKorisnika w = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.DODAJ);

                if (w.ShowDialog() == true)
                {
                    pacijent.Adresa = w.adresa;
                }
            }
            else
            {
                DodajIzmijeniAdresuKorisnika izmijeni = new DodajIzmijeniAdresuKorisnika(poljaAdrese, DodajIzmijeniAdresuKorisnika.Opcija.IZMIJENI);
                izmijeni.ShowDialog();
            }
        }
    }
}
