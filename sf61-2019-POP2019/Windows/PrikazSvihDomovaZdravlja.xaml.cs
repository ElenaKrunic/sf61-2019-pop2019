﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihDomovaZdravlja.xaml
    /// </summary>
    public partial class PrikazSvihDomovaZdravlja : Window
    {
        ICollectionView view;
        string searchFilter = "Active";

        public PrikazSvihDomovaZdravlja()
        {
            InitializeComponent();

            UpdateView();
        }

        private bool CustomFilter(object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            if (searchFilter.Equals("SEARCH") && String.IsNullOrEmpty(TxtNazivDomaZdravlja.Text))
            {
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                       && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            }
            if (searchFilter.Equals("SEARCH"))
                return (domZdravlja.Aktivan && domZdravlja.NazivInstitucije.ToLower().Contains(TxtNazivDomaZdravlja.Text.ToLower()))
                    && (domZdravlja.Aktivan && domZdravlja.Adresa.Ulica.ToLower().Contains(txtUlica.Text.ToLower()));
            return domZdravlja.Aktivan;
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.DomoviZdravlja);
            DGDomoviZdravlja.IsSynchronizedWithCurrentItem = true;
            DGDomoviZdravlja.ItemsSource = view;

            DGDomoviZdravlja.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }

        private void DGDomoviZdravlja_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("Ljekari") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("ZaposleniLjekari"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            } 
        }

        private void TxtNazivDomaZdravlja_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DodajUstanovuBtn_Click(object sender, RoutedEventArgs e)
        {
            DomZdravlja noviDomZdravlja = new DomZdravlja();
            DodajIzmijeniDomZdravlja dodajDomZdravlja = new DodajIzmijeniDomZdravlja(noviDomZdravlja, DodajIzmijeniDomZdravlja.Opcija.DODAJ) ;
            dodajDomZdravlja.ShowDialog();
        }

        private void IzmijeniUstanovuBtn_Click(object sender, RoutedEventArgs e)
        {
            
            DomZdravlja selektovanDomZdravlja = view.CurrentItem as DomZdravlja;
            if (selektovanDomZdravlja != null)
            {
                DomZdravlja stariDomZdravlja = selektovanDomZdravlja.Clone() as DomZdravlja;
                DodajIzmijeniDomZdravlja izmijeniDomZdravlja = new DodajIzmijeniDomZdravlja(selektovanDomZdravlja, DodajIzmijeniDomZdravlja.Opcija.IZMIJENI);

                if(izmijeniDomZdravlja.ShowDialog() != true)
                {
                    int index = Util.Instance.DomoviZdravlja.IndexOf(stariDomZdravlja);
                    Util.Instance.DomoviZdravlja[index] = stariDomZdravlja;
                }
            }
        }

        private void ObrisiUstanovuBtn_Click(object sender, RoutedEventArgs e)
        {
            DomZdravlja zaBrisanje = view.CurrentItem as DomZdravlja;

            if(zaBrisanje != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    DomZdravlja.ObrisiDomZdravlja(zaBrisanje);
                }
            }
        }

        private void OtkaziUstanovuBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TxtAdresa_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }
    }
}
