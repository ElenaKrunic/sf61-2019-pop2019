﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for DodajIzmijeniAdministratora.xaml
    /// </summary>
    public partial class DodajIzmijeniAdministratora : Window
    {
        public enum Opcija { DODAJ, IZMIJENI }
        Administrator administrator;
        Opcija opcija; 
        public DodajIzmijeniAdministratora(Administrator administrator, Opcija opcija)
        {
            InitializeComponent();
            this.administrator = administrator;
            this.opcija = opcija;

            txtIme.DataContext = administrator;
            txtPrezime.DataContext = administrator;
            txtKorisnickoIme.DataContext = administrator;
            txtEmail.DataContext = administrator;
            txtKorisnickoIme.DataContext = administrator;
            txtPassword.DataContext = administrator;
            txtJMBG.DataContext = administrator;

            if(opcija.Equals(Opcija.IZMIJENI))
            {
                txtJMBG.IsEnabled = false; 
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true; 

            if(administrator.ID != 0)
            {
                Administrator.IzmijeniAdministrator(administrator);
            } else
            {
                Util.Instance.Administratori.Add(administrator);
                Administrator.DodajAdministratora(administrator);
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
