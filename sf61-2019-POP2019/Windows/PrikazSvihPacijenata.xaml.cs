﻿using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf61_2019_POP2019.Windows
{
    /// <summary>
    /// Interaction logic for PrikazSvihPacijenata.xaml
    /// </summary>
    public partial class PrikazSvihPacijenata : Window
    {
        ICollectionView view;
        string searchFilter = "Active";

        public PrikazSvihPacijenata()
        {
            InitializeComponent();

            cmbTipKorisnika.ItemsSource = new List<ETipKorisnika> { ETipKorisnika.ADMINISTRATOR, ETipKorisnika.LEKAR, ETipKorisnika.PACIJENT };
            DGPacijenti.IsReadOnly = true;

            UpdateView();

        }
     
        public void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Pacijenti);
            DGPacijenti.IsSynchronizedWithCurrentItem = true;
            DGPacijenti.ItemsSource = view;
            DGPacijenti.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Auto);
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            if (searchFilter.Equals("SEARCH") && cmbTipKorisnika.SelectedIndex > -1)
                return ((korisnik.Aktivan &&
                        korisnik.Prezime.ToLower().Contains(txtPrezime.Text.ToLower()) &&
                        korisnik.Ime.ToLower().Contains(TxtBoxImePacijenta.Text.ToLower()) &&
                        korisnik.Email.ToLower().Contains(txtEmail.Text.ToLower()) &&
                        korisnik.Adresa.Ulica.ToLower().Contains(txtAdresa.Text.ToLower()) &&
                        korisnik.TipKorisnika.ToString().ToLower().Equals(cmbTipKorisnika.SelectedItem.ToString().ToLower())));
            else if (searchFilter.Equals("SEARCH"))
                return ((korisnik.Aktivan &&
                        korisnik.Ime.ToLower().Contains(TxtBoxImePacijenta.Text.ToLower()) &&
                 korisnik.Prezime.ToLower().Contains(txtPrezime.Text.ToLower()) &&
                 korisnik.Email.ToLower().Contains(txtEmail.Text.ToLower())));
            else
                return korisnik.Aktivan;
        }


        private void DGPacijenti_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Aktivan") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ErrorCollection") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("ListaZakazanihTermina") || e.PropertyName.Equals("ListaTerapija"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void TxtBoxImePacijenta_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnDodajPacijenta_Click(object sender, RoutedEventArgs e)
        {
            Pacijent pacijent = new Pacijent();
            DodajIzmijeniPacijenta dodajPacijenta = new DodajIzmijeniPacijenta(pacijent, DodajIzmijeniPacijenta.Opcija.DODAJ);
            dodajPacijenta.ShowDialog();
        }

        private void BtnIzmijeniPacijenta_Click(object sender, RoutedEventArgs e)
        {
            Pacijent selektovanPacijent = view.CurrentItem as Pacijent; 
            if(selektovanPacijent != null)
            {
                Pacijent stariPacijent = selektovanPacijent.Clone() as Pacijent;

                DodajIzmijeniPacijenta izmijeniPacijenta = new DodajIzmijeniPacijenta(selektovanPacijent, DodajIzmijeniPacijenta.Opcija.IZMIJENI);

                if(izmijeniPacijenta.ShowDialog() != true)
                {
                    int index = Util.Instance.Pacijenti.IndexOf(selektovanPacijent);
                    Util.Instance.Pacijenti[index] = stariPacijent;
                }
            }
        }

        private void BtnObrisiPacijenta_Click(object sender, RoutedEventArgs e)
        {

            if (view.CurrentItem is Pacijent selektovanPacijent)
            {
                if (MessageBox.Show("Da li ste sigurni? ", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Pacijent.ObrisiPacijenta(selektovanPacijent);
                }
            }
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            searchFilter = "SEARCH";
            view.Refresh();
        }

        private void txtPrezime_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtEmail_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtAdresa_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void cmbTipKorisnika_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }

}
