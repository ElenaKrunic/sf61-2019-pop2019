﻿namespace sf61_2019_POP2019.Services
{
    public interface IDoctorService
    {
        // IDoctorService interfejs za : 
        //vrati dom zdravlja 
        //vrati listu pacijenata 
        void saveDomZdravlja(string filename);
        void readDomZdravlja(string filename);
        void deleteDomZdravlja(string nazivInstitucije);
        void saveTerapija(string filename);
        void readTerapija(string filename);
        void deleteTerapija(string sifraTerapije);
    }
}
