﻿using sf61_2019_POP2019.Exceptions;
using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Services
{
    public class UserService
    {

        /*
        public void ReadUsers(string filename)
        {
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();

            using(StreamReader file = new StreamReader(@"../../Files/" + filename))
            {
                string line; 
                while ((line = file.ReadLine()) != null)
                {
                    string[] korisnikIzFajla = line.Split(';');

                    Enum.TryParse(korisnikIzFajla[6], out EPol pol);
                    Enum.TryParse(korisnikIzFajla[7], out ETipKorisnika tip);
                    Boolean.TryParse(korisnikIzFajla[12], out Boolean status);

                    Adresa adresa = new Adresa
                    {
                        Drzava = korisnikIzFajla[8],
                        Broj = korisnikIzFajla[9],
                        Ulica = korisnikIzFajla[10],
                        Grad = korisnikIzFajla[11]
                    };

                    Korisnik korisnik = new Korisnik
                    {
                        KorisnickoIme = korisnikIzFajla[0],
                        Ime = korisnikIzFajla[1],
                        Prezime = korisnikIzFajla[2],
                        JMBG = korisnikIzFajla[3],
                        Email = korisnikIzFajla[4],
                        Lozinka = korisnikIzFajla[5],
                        Pol = pol,
                        TipKorisnika = tip,
                        Adresa = adresa,
                        Aktivan = status
                    };
                    Util.Instance.Korisnici.Add(korisnik);
                }
            }
        } 

        public void ReadAdresa(string filename)
        {
            Util.Instance.Adrese = new ObservableCollection<Adresa>(); 

            using(StreamReader file = new StreamReader(@"../../Files/" + filename))
            {
                String line; 
                while((line = file.ReadLine()) != null)
                {
                    string[] adresaIzFajla = line.Split(';');
                    Boolean.TryParse(adresaIzFajla[5], out Boolean status);
                    Adresa adresa = new Adresa { ID = adresaIzFajla[0], Ulica = adresaIzFajla[1], Broj = adresaIzFajla[2], Grad = adresaIzFajla[3], Drzava = adresaIzFajla[4], Aktivan=status };
                    Util.Instance.Adrese.Add(adresa);
                }
            }
        }

        
        public void SaveUsers(string filename)
        {
            using (StreamWriter file = new StreamWriter(@"../../Files/" + filename))
            {
                foreach(Korisnik korisnik in Util.Instance.Korisnici)
                {
                    file.WriteLine(korisnik.KorisnikZaUpisUFajl());    
                }
            }
        }
        

        public void DeleteUser(string username)
        {
            Korisnik k = Util.Instance.Korisnici.ToList().Find(kori => kori.KorisnickoIme.Equals(username));

            if (k == null)
            {
                throw new UserNotFoundException($"Ne postoji korisnik sa korisnickim imenom {username}");
            }
            k.Aktivan = false;

            DeleteUserFromDb(k);
        }

        public void SacuvajAdresu(string filename)
        {
            using (StreamWriter file = new StreamWriter(@"../../Files/" + filename))
            {
                foreach (Adresa adresa in Util.Instance.Adrese)
                {
                    file.WriteLine(adresa.AdresaZaUpisUFajl());
                }
            }
        }

        //-------------------------------------------------- ZA RAD SA BAZOM ----------------------------------------------------------

        public void ReadUsers()
        {
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();

            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                //pisem upit 
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where TypeOfUser like 'LEKAR' ";

                //koristim komandu select, znaci ne mogu da izvrsim upit pomocu EXECUTE jer nema izvrsavanja, samo citanje 
                //zato SqlDataReader 
                SqlDataReader reader = command.ExecuteReader();

                while(reader.Read())
                {
                    Util.Instance.Korisnici.Add(new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1),
                        Ime = reader.GetString(2),
                        TipKorisnika = ETipKorisnika.LEKAR, 
                        Email = reader.GetString(4),
                        Aktivan = reader.GetBoolean(5)
                    });
                }

                reader.Close();
            }
        }

        public int SaveUser(object obj)
        {
            Korisnik korisnik = obj as Korisnik; 

            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                //ovo output inserted pisem da bi mi se vratio id novog korisnika 

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Users(Username,Firstname,TypeOfUser,Email,Active)  
                    output inserted.id VALUES(@Username, @Firstname, @TypeOfUser, @Email, @Active)";

                command.Parameters.Add(new SqlParameter("Username", korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Firstname", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("TypeOfUser", korisnik.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("Active", korisnik.Aktivan));

                //hocu da mi se vrati vrijednost id koji je izvrsen 
                return (int) command.ExecuteScalar();
            }

           // return -1; 
        }

        public void DeleteUserFromDb(object obj)
        {
            Korisnik korisnik = obj as Korisnik; 

            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Users SET active = @Active where username= @Username";
                command.Parameters.Add(new SqlParameter("Active", korisnik.Aktivan));
                command.Parameters.Add(new SqlParameter("Username", korisnik.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public void UpdateUser(object obj)
        {
            Korisnik korisnik = obj as Korisnik; 

            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.users set active=@Active, 
                                        firstname=@Firstname, typeOfUser=@TypeOfUser, email=@Email
                                        where username=@Username";

                command.Parameters.Add(new SqlParameter("@Active", korisnik.Aktivan));
                command.Parameters.Add(new SqlParameter("@Firstname", korisnik.Ime));
                command.Parameters.Add(new SqlParameter("@TypeOfUser", ETipKorisnika.LEKAR));
                command.Parameters.Add(new SqlParameter("@Email", korisnik.Email));
                command.Parameters.Add(new SqlParameter("@Username", korisnik.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }
    }
        */

    }
}

