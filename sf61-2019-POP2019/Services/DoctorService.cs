﻿using sf61_2019_POP2019.Exceptions;
using sf61_2019_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf61_2019_POP2019.Services
{
    //klasa nizeg nivoa zato sto vodi racuna o upisu u bazu, citanje iz fajla itd.. 

    /*
    public class DoctorService : IUserService, IDoctorService
    {

        
        public void SaveUsers(string filename)
        {
            using (StreamWriter file = new StreamWriter(@"../../Files/" + filename))
            {
                foreach(Lekar lekar in Util.Instance.Lekari)
                {
                    file.WriteLine(lekar.LekarZaUpisUFajl());
                }
            }
        }
        

        public void saveDomZdravlja(string filename)
        {
            using(StreamWriter file = new StreamWriter(@"../../Files/" + filename))
            {
                foreach(DomZdravlja domZdravlja in Util.Instance.DomoviZdravlja)
                {
                    file.WriteLine(domZdravlja.DomZdravljaZaUpisUFajl());
                }
            }
        }

        public void saveTerapija(string filename)
        {
            using (StreamWriter file = new StreamWriter(@"../../Files/" + filename))
            {
                foreach (Terapija terapija in Util.Instance.Terapije)
                {
                    file.WriteLine(terapija.TerapijaZaUpisUFajl());
                }
            }
        }

        
        public void ReadUsers(string filename)
        {
            Util.Instance.Lekari = new ObservableCollection<Lekar>();
            using (StreamReader file = new StreamReader(@"../../Files/" + filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] lekarIzFajla = line.Split(';');
                    //Korisnik korisnik = NadjiKorisnika(lekarIzFajla[1]);
                    Korisnik korisnik = Util.Instance.Korisnici.ToList().Find(kor => kor.KorisnickoIme.Equals(lekarIzFajla[1]));
                    // Korisnik >>> Lekar 
                    Lekar lekar = new Lekar
                    {
                        DomZdravlja = lekarIzFajla[0],
                        ReferencaNaKorisnika = korisnik
                    };

                    Util.Instance.Lekari.Add(lekar);
                }
            }
        }
        

        
        public void DeleteUser(string username)
        {
            
            Lekar lk = Util.Instance.Lekari.ToList().Find(lek => lek.ReferencaNaKorisnika.KorisnickoIme.Equals(username));

            if (lk == null)
            {
                throw new UserNotFoundException($"Ne postoji korisnik sa korisnickim imenom {username}");
            }

            lk.ReferencaNaKorisnika.Aktivan = false;
            
        }
    
        public void DeleteUser(string username)
        {
            throw new NotImplementedException();
        }


        public void readDomZdravlja(string filename)
        {
            Util.Instance.DomoviZdravlja = new ObservableCollection<DomZdravlja>(); 
            using(StreamReader file = new StreamReader(@"../../Files/" + filename))
            {
                string line; 
                while((line = file.ReadLine()) != null)
                {
                    string[] domZdravljaIzFajla = line.Split(';');
                    Boolean.TryParse(domZdravljaIzFajla[6], out Boolean status);

                    Adresa adresa = new Adresa
                    {
                        Drzava = domZdravljaIzFajla[2],
                        Broj = domZdravljaIzFajla[3],
                        Ulica = domZdravljaIzFajla[4],
                        Grad = domZdravljaIzFajla[5]
                    };

                    DomZdravlja domZdravlja = new DomZdravlja
                    {
                        Sifra = domZdravljaIzFajla[0],
                        NazivInstitucije = domZdravljaIzFajla[1],
                        AdresaDomaZdravlja = adresa,
                        Aktivan = status
                    };
                    Util.Instance.DomoviZdravlja.Add(domZdravlja);
                }
            }
        }
        public void readTerapija(string filename)
        {
            Util.Instance.Terapije = new ObservableCollection<Terapija>(); 

            using(StreamReader file = new StreamReader(@"../../Files/" + filename))
            {
                string line; 
                while((line = file.ReadLine()) != null)
                {

                    string[] terapijeIzFajla = line.Split(';');

                    Boolean.TryParse(terapijeIzFajla[3], out Boolean status);
                    Terapija terapija = new Terapija
                    {
                        SifraTerapije = terapijeIzFajla[0],
                        OpisTerapije = terapijeIzFajla[1],
                        LekarKojiJePrepisaoTerapiju = terapijeIzFajla[2],
                        Aktivan = status
                    };

                    Util.Instance.Terapije.Add(terapija);
                }
            }
        }

        public void deleteDomZdravlja(string nazivInstitucije)
        {
            DomZdravlja domZdravlja = Util.Instance.DomoviZdravlja.ToList().Find(d => d.NazivInstitucije.Equals(nazivInstitucije)); 

            if (domZdravlja == null)
            {
                // prilagodi
                throw new DomZdravljaNotFoundException($" Ne postoji Dom zdravlja pod nazivom {nazivInstitucije}");

            }

            domZdravlja.Aktivan = false;
            //Util.Instance.SacuvajEntitete("domZdravlja.txt");

        }

        public void deleteTerapija(string sifraTerapije)
        {
            Terapija t = Util.Instance.Terapije.ToList().Find(ter => ter.SifraTerapije.Equals(sifraTerapije));

            if(t==null)
            {
                throw new Exception();
            }

            t.Aktivan = false;
            //Util.Instance.SacuvajEntitete("terapije.txt");
        }

        public void SacuvajAdresu(string filename)
        {
            throw new NotImplementedException();
        }

        public void ReadAdresa(string filename)
        {
            throw new NotImplementedException();
        }

        //------------------------------------------------------- ZA RAD SA BAZOM -------------------------------------------------------------------

        public void ReadUsers()
        {
            Util.Instance.Lekari = new ObservableCollection<Lekar>();
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>(); 

            using(SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where TypeOfUser like 'LEKAR' ";

                SqlDataReader reader = command.ExecuteReader(); 

                while(reader.Read())
                {
                    Util.Instance.Korisnici.Add(new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1), 
                        Ime = reader.GetString(2),
                        TipKorisnika = ETipKorisnika.LEKAR, 
                        Email = reader.GetString(4), 
                        Aktivan = reader.GetBoolean(5)
                    });
                }

                reader.Close();
            }
        }


        
        public void ReadUsers()
        {
            Util.Instance.Lekari = new ObservableCollection<Lekar>();
            Util.Instance.Korisnici = new ObservableCollection<Korisnik>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                string selectedUsers = @"select * from users where TypeOfUser like 'LEKAR' ";
                SqlDataAdapter adapter = new SqlDataAdapter(selectedUsers, conn);

                DataSet dataSet = new DataSet();

                //Lekari je tabela kreirana 
                adapter.Fill(dataSet, "Lekari");

                foreach (DataRow row in dataSet.Tables["Lekari"].Rows)
                {
                    Util.Instance.Korisnici.Add(new Korisnik
                    {
                        ID = Convert.ToInt32(row["Id"]),
                        KorisnickoIme = (string) row["Username"],
                        Ime = (string) row["Firstname"],
                        TipKorisnika = ETipKorisnika.LEKAR,
                        Email = (string)row["Email"],
                        Aktivan = (bool)row["Active"]
                    });
                }


                
                //pisem upit 
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where TypeOfUser like 'LEKAR' ";

                //koristim komandu select, znaci ne mogu da izvrsim upit pomocu EXECUTE jer nema izvrsavanja, samo citanje 
                //zato SqlDataReader 
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Util.Instance.Korisnici.Add(new Korisnik
                    {
                        ID = reader.GetInt32(0),
                        KorisnickoIme = reader.GetString(1),
                        Ime = reader.GetString(2),
                        TipKorisnika = ETipKorisnika.LEKAR,
                        Email = reader.GetString(4),
                        Aktivan = reader.GetBoolean(5)
                    });
                }

                reader.Close();
               
            }
        }
         

        public int SaveUser(object obj)
        {
            Lekar lekar = obj as Lekar;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                //ovo output inserted pisem da bi mi se vratio id novog korisnika 

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Doctors (Id)  
                    output inserted.id VALUES(@Id)";

                command.Parameters.Add(new SqlParameter("Id", lekar.ID));

                //hocu da mi se vrati vrijednost id koji je izvrsen 
                return (int)command.ExecuteScalar();

            }

            // return -1; 
        }

        public void UpdateUser(object obj)
        {
            throw new NotImplementedException();
        }

        public void DeleteUserFromDb(object obj)
        {
            throw new NotImplementedException();
        }
    */
    }


