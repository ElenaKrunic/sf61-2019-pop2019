﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sf61_2019_POP2019.Validations
{
    //dodati regex 
    class EmailValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            if (value.ToString().Contains("@") && value.ToString().EndsWith(".com"))
                return new ValidationResult(true, "Prosla validacija");
            return new ValidationResult(false, "Nije prosla validacija");
        }
    }
}
