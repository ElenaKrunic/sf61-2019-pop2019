﻿--bit ima vrijednosti 0 i 1
create table [dbo].[Users] (
[Id] int not null IDENTITY(1,1) primary key, 
[Username] varchar(255) not null, 
[Firstname] varchar(255) not null, 
[TypeOfUser] varchar(255) not null, 
[Email] varchar(255) not null, 
[Active] bit not null
)

create table [dbo].[Doctors] (
	[Id] int not null primary key, 
	--[DOM_ZDRAVLJA_ID] int not null,
	constraint fk_User_Doctor
		foreign key (ID) references dbo.Users (ID),
	--constraint fk_DomZdravlja_Doctor
		--foreign key (DOM_ZDRAVLJA_ID) references dbo.DomZdravlja (ID)
)

create table [dbo].[Pacients] (
	[Id] int not null primary key, 
	constraint fk_User_Pacient
		foreign key (ID) references dbo.Users (ID), 
)
