﻿drop table Doctors;

create table DomZdravlja (
	ID int not null identity(1,1) primary key, 
	NazivInstitucije varchar(255) not null,
	Aktivan bit not null
);

create table Lekari(
	ID int not null identity(1,1) primary key, 
	Ime varchar(255) not null, 
	Prezime varchar(255) not null, 
	KorisnickoIme varchar(255) not null,
	Email varchar(255) not null, 
	Lozinka varchar(255) not null,
	Aktivan bit not null,
	JMBG varchar(255) not null,
	Pol int null, 
	TipKorisnika int null,
	IDDomaZdravlja int foreign key references DomZdravlja(ID)
);