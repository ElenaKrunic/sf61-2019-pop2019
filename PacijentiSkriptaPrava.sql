﻿create table Pacijenti(
	ID int not null identity(1,1) primary key, 
	Ime varchar(255) not null, 
	Prezime varchar(255) not null, 
	KorisnickoIme varchar(255) not null, 
	Email varchar(255) not null, 
	Lozinka varchar(255) not null, 
	Aktivan bit not null,
	JMBG varchar(255) not null,
	Pol int null, 
	TipKorisnika int null,
	IDLekara int foreign key references Lekari(ID)
);