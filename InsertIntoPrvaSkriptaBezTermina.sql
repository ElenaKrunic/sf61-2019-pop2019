﻿--prvo adrese
insert into Adrese(Ulica,Broj,Grad,Drzava,Aktivan) values ('Karadjordjeva ', 'BB', 'Teslic', 'BiH', 1);
insert into Adrese(Ulica,Broj,Grad,Drzava,Aktivan) values ('Branka Bajica', '9E','Novi Sad','Srbija',1);
insert into Adrese(Ulica,Broj,Grad,Drzava,Aktivan) values ('Rosuljska', '56','Banja Luka','BiH',1);
insert into Adrese(Ulica,Broj,Grad,Drzava,Aktivan) values ('1300 kaplara', '122','Beograd','Srbija',1);

select * from Adrese;

insert into DomZdravlja(NazivInstitucije,Aktivan,IDAdrese) values ('Sveti Sava', 1, 2017);
insert into DomZdravlja(NazivInstitucije,Aktivan,IDAdrese) values ('Dom Zdravlja Novi Sad', 1, 2019);
insert into DomZdravlja(NazivInstitucije,Aktivan,IDAdrese) values ('Paprikovac', 1, 2020);
insert into DomZdravlja(NazivInstitucije,Aktivan,IDAdrese) values ('Dom Zdravlja Stari Grad', 1, 2021);

select * from DomZdravlja;

insert into Lekari(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDDomaZdravlja, IDAdrese) 
values ('Dragan','Kostic','Gandra','draganKostic@gmail.com','asdf123',1,'1010979125025',0,1,1002,2018); 
insert into Lekari(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDDomaZdravlja, IDAdrese) 
values ('Lena','Ristic','Lele','lelekrunic1@gmail.com','asdf123',1,'1010999125025',1,1,1004,2020); 
insert into Lekari(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDDomaZdravlja, IDAdrese) 
values ('Mihailo','Nikolic','Mica','mica55@gmail.com','asdf123',1,'1010999125025',0,1,1006,2021); 

select * from Lekari;

insert into Pacijenti(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDLekara, IDAdrese) 
values ('Mirela','Svetic','Mirela','sveticsvetic@gmail.com','asdf123',1,'1010979125025',1,2,2002,2018); 
insert into Pacijenti(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDLekara, IDAdrese) 
values ('Anja','Lelic','Anja','anjalelekic1990@gmail.com','asdf123',1,'1010999125025',1,2,2004,2021); 
insert into Pacijenti(Ime,Prezime,KorisnickoIme,Email,Lozinka,Aktivan,JMBG,Pol,TipKorisnika,IDLekara, IDAdrese) 
values ('Petar','Zivkovic','Rope','zivkovic18@gmail.com','asdf123',1,'1010999125025',0,2,2005,2020); 

select * from Pacijenti;

insert into Terapije(OpisTerapije, Aktivan,IDLekara) values ('2xdnevno',1,2002);
insert into Terapije(OpisTerapije, Aktivan,IDLekara) values ('4xdnevno',1,2004);
insert into Terapije(OpisTerapije, Aktivan,IDLekara) values ('10xmjesecno',1,2005);

select * from Terapije;
