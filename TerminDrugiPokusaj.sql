﻿create table Termini (
ID int not null identity(1,1), 
DatumTermina datetime not null,
StatusTermina nvarchar(225) not null,
Aktivan bit not null,
DomZdravljaID int foreign key references DomZdravlja(ID),
LjekarID int foreign key references Lekari(ID), 
PacijentID int foreign key references Pacijenti(ID) 
);