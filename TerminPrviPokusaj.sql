﻿--ID, datumTermina, status,pacijent,ljekar, aktivan 
create table Termin (
ID int not null identity (1,1), 
DatumTermina datetime not null,
StatusTermina int not null,
Aktivan bit not null, 
LjekarID int foreign key references Lekari(ID), 
PacijentID int foreign key references Pacijenti(ID)
);