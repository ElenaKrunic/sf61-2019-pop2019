﻿create table Adrese (
ID int not null identity(1,1) primary key, 
Ulica varchar(255) not null, 
Broj varchar(255) not null, 
Grad varchar(255) not null,
Drzava varchar(255) not null,
Aktivan bit not null
);

ALTER TABLE table_name
ADD column_name datatype;

alter table Lekari 
add IDAdrese int foreign key references Adrese(ID)

--IDInstitution int FOREIGN KEY REFERENCES Institution(ID)