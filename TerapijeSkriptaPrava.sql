﻿create table Terapije(
ID int not null identity(1,1) primary key, 
OpisTerapije varchar(255) not null, 
Aktivan bit not null, 
IDLekara int foreign key references Lekari(ID)
);