﻿create table DomZdravlja(
ID int not null identity(1,1) primary key, 
NazivInstitucije varchar(255) not null, 
Aktivan bit not null
);

create table Adrese (
ID int not null Identity(1,1) primary key, 
Ulica varchar(255) not null, 
Broj varchar(4) not null,
Grad varchar(255) not null,
Drzava varchar(255) not null, 
Aktivan bit not null
);

create table Lekari(
ID int not null identity(1,1) primary key, 
Ime varchar(255) not null, 
Prezime varchar(255) not null, 
KorisnickoIme varchar(255) not null,
Email varchar(255) not null, 
Lozinka varchar(255) not null,
Aktivan bit not null,
JMBG varchar(255) not null,
Pol int null, 
TipKorisnika int null,
IDDomaZdravlja int foreign key references DomZdravlja(ID)
);

create table Pacijenti(
ID int not null identity(1,1) primary key, 
Ime varchar(255) not null, 
Prezime varchar(255) not null, 
KorisnickoIme varchar(255) not null, 
Email varchar(255) not null, 
Lozinka varchar(255) not null, 
Aktivan bit not null,
JMBG varchar(255) not null,
Pol int null, 
TipKorisnika int null,
IDLekara int foreign key references Lekari(ID)
);

create table Terapije(
ID int not null identity(1,1) primary key, 
OpisTerapije varchar(255) not null, 
Aktivan bit not null, 
IDLekara int foreign key references Lekari(ID)
);
