﻿create table Administratori (
ID int not null identity(1,1) primary key, 
Ime varchar(255) not null,
Prezime varchar(255) not null, 
KorisnickoIme varchar(255) not null, 
Email varchar(255) not null,
Lozinka varchar(255) not null,
Aktivan bit not null, 
JMBG varchar(255) not null,
Pol int null,
TipKorisnika int null
);

insert into Administratori (Ime,Prezime,KorisnickoIme, Email, Lozinka, Aktivan, JMBG, Pol,TipKorisnika) values 
('Elena', 'Krunic','Ele', 'elenakrunic@gmail.com', 'lelepanda',1,'1010999125025',1,0);

select * from Administratori; 